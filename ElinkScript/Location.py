from ElinkScript.Debug import *
from ElinkScript import Region

class Location(Region):
    x_rel = 0
    y_rel = 0

    def __init__(self, x, y, x_rel = 0, y_rel = 0):
        self.x = x
        self.y = y
        self.x_rel = x_rel
        self.x_rel = y_rel

    def setX(self, x):
        self.x = x

    def setY(self, y):
        self.y = y

    def getX(self):
        return self.x

    def getY(self):
        return self.y

    def getXRelative(self):
        return self.x_rel

    def getYRelative(self):
        return self.y_rel

    def getW():
        return 0

    def getH():
        return 0

    def setW(w):
        self.w = 0

    def setH(h):
        self.h = 0
