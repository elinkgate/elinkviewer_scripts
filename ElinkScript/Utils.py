import re
import sys,os
import ElinkScript

def normalize_path(path):
    return os.path.normpath(os.sep.join(re.split(r'\\|/', path)))
