import ElinkScript
from ElinkScript.Debug import *
from time import time as _time, sleep


class Region:
    x = 0
    y = 0
    w = 0
    h = 0

    def __init__(self, *args, **kwargs):
        if len(args) == 4:
            self.x = args[0]
            self.y = args[1]
            self.w = args[2]
            self.h = args[3]
        elif len(args) == 3:
            point = args[0]
            self.x = point.getX()
            self.y = point.getY()
            self.w = args[1]
            self.h = args[2]

    def setX(self, x):
        self.x = x

    def setY(self, y):
        self.y = y

    def setW(self, w):
        self.w = w

    def setH(self, h):
        self.h = h

    def getX(self):
        return self.x

    def getY(self):
        return self.y

    def getW(self):
        return self.w

    def getH(self):
        return self.h

    def getCenter(self):
        half_w = self.w / 2
        half_h = self.h / 2
        x_center = round(self.x + half_w)
        y_center = round(self.y + half_h)
        return ElinkScript.Location(x_center, y_center)

    def getTopLeft(self):
        return ElinkScript.Location(self.x, self.y)

    def getTopRight(self):
        return ElinkScript.Location(self.x + w, self.y)

    def getBottomLeft(self):
        return ElinkScript.Location(self.x, self.y + self.h)

    def getBottomRight(self):
        return ElinkScript.Location(self.x + self.w, self.y + h)

    def click(self, delay=2):
        dbg("x: " + str(x) + " y: " + str(y))
        dbg("x_abs: " + str(x_abs) + " y_abs: " + str(y_abs))
        self.vnc.sendMouse(0, x, y)
        sleep(1)
        # right mouse click at 100,100	(or 0x40)
        self.vnc.sendMouse("LDOWN|CLICK", x, y)
        sleep(delay)

    def clickUntil(self, pattern):
        try:
            dbg("Click until " + pattern.getFileName())
        except AttributeError:
            pass

        while not pattern.exists(500):
            self.click()
        try:
            dbg("Found image: " + pattern.getFileName())
        except AttributeError:
            pass

    def waitClick(self):
        r = self.waitImage(self)
        # print("r: " + r)
        print("Found at: " + str(r.getX()) + ", " + str(r.getY()))
        print("Width: " + str(r.getW()) + ", Height: " + str(r.getY()))
        r.click()

    def waitImage(self, pattern, timeout=0, attentionPeriod=3000):
        vnc = ElinkScript.ElinkSystem.vnc
        img = pattern.getFileName()
        score = pattern.getScore()

        print("wait for " + repr(img) + ", timeout: " + str(timeout))
        time_spent = 0
        while True:
            time1 = int(round(_time() * 1000))
            loc = vnc.matchScreen(img, score, attentionPeriod)
            dbg("max matching %3.2f" % loc[1])

            if loc[0] != None:
                dbg(img + " matching at " + repr(loc[0]))
                pattern.setMatchedScore(loc[1])
                pattern.setX(loc[0][0])
                pattern.setY(loc[0][1])
                pattern.setW(loc[0][2])
                pattern.setH(loc[0][3])

                sleep((attentionPeriod+500)/1000)
                return pattern

            sleep(0.1)

            time2 = int(round(_time() * 1000))
            time_spent += 0.1*1000 + (time2 - time1)

            if timeout > 0 and timeout <= time_spent:
                sleep((attentionPeriod+500)/1000)
                break

        return None

    def waitImagesOr(self, img_list, attentionPeriod=3000):
        vnc = ElinkScript.ElinkSystem.vnc

        dbg("Wait for: ")
        for p in img_list:
            dbg(repr(p.getFileName()) + " ")

        while True:
            for pattern in img_list:
                img = pattern.getFileName()
                score = pattern.getScore()
                found = vnc.matchScreen(img, score, attentionPeriod)
                dbg("max matching %3.2f" % found[1])
                if found[0]:
                    dbg("matching at " + repr(found[0]))
                    pattern.setX(found[0][0])
                    pattern.setY(found[0][1])
                    pattern.setMatchedScore(found[1])
                    sleep((attentionPeriod+500)/1000)
                    return pattern

                sleep(2)
        return None

    def click(self, delay=None):
        vnc = ElinkScript.ElinkSystem.vnc
        if delay is None:
            delay = 2

        # dbg("x_abs: " + str(self.x_abs) + " y_abs: " + str(self.y_abs))
        l = self.getCenter()
        dbg("x: " + str(l.getX()) + " y: " + str(l.getY()))
        vnc.sendMouse(0, l.getX(), l.getY())
        sleep(1)
        # right mouse click at 100,100	(or 0x40)
        vnc.sendMouse("LDOWN|CLICK", l.getX(), l.getY())
        sleep(delay)
