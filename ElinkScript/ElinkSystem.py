import sys
import os
import ElinkScript
from ElinkScript import Region
from ElinkScript import Utils


class ELinkInfo:
    def __init__(self, devinfo: list):
        if len(devinfo) == 0:
            return
        self.ipAddr = devinfo[0]
        self.devId = devinfo[1]
        # self.devMacAddr = devinfo[2]

    def get_dev_id(self):
        return self.devId

    def get_dev_ip(self):
        return self.ipAddr

    def get_mac_addr(self):
        return


class ElinkSystem:
    elink = None
    vnc = None
    screen = None

    # def __init__(self, elink, ip):
    #     self.elink = elink
    #     self.vnc = self.connect(ip)

    @staticmethod
    def getScriptPath():
        env = os.environ.copy()
        return Utils.normalize_path(os.getcwd() + "/" + os.path.dirname(env["ELINKKVM_SCRIPTDIR"]))

    @staticmethod
    def connect(elink, ip):
        # os.chdir(scriptPath)
        # print("cwd changed: " + ElinkSystem.getScriptPath())

        ElinkSystem.elink = elink
        ElinkSystem.vnc = ElinkSystem.elink.newConnection(ip)
        ElinkSystem.screen = ElinkSystem.getScreen()
        sys.path.append(ElinkSystem.getScriptPath())

        return ElinkSystem.vnc

    @staticmethod
    def scanDevices():
        raw_devs_info = ElinkSystem.elink.scanDevice()
        devs_info = []
        for e in raw_devs_info:
            devs_info.append(ELinkInfo(e))

        return devs_info

    @staticmethod
    def getScreen():
        info = ElinkSystem.vnc.info()
        # dbg(info)
        w = info[3]
        h = info[4]

        r = Region(0, 0, w, h)

        return r

    @staticmethod
    def getConnection():
        return ElinkSystem.vnc

    @staticmethod
    def sendCommand():
        ElinkSystem.vnc.sendString(command)
        sleep(1)
        ElinkSystem.vnc.sendKey("Enter")
        sleep(1)

    @staticmethod
    def waitKeyReady():
        print("waitKeyboard")
        key_state = -1

        while True:
            ev = ElinkSystem.vnc.getEvent()
            if ev.getIdCode() == 2:
                break

        ev = ElinkSystem.vnc.getEvent(2000)

        while True:
            if ev.getIdCode() == 6:
                key_state = ev.getData("Test") & 0x04
                break
            elif ev.getIdCode() == -1:
                break
            else:
                ev = ElinkSystem.vnc.getEvent(2000)

        if key_state == -1:
            key_state = 0
            ElinkSystem.vnc.sendKey("ScrollLock")

        count_send = 0
        while True:
            ev = ElinkSystem.vnc.getEvent(2000)
            if ev.getIdCode() == -1:
                count_send = count_send + 1
                if count_send < 3:
                    ElinkSystem.vnc.sendKey("ScrollLock")
                    continue
                else:
                    print("Keyboard is not ready")
                    assert 0
            elif ev.getIdCode() == 6:
                key_state2 = ev.getData("Test") & 0x04
            else:
                continue

            if key_state2 == key_state:
                print("Keyboard is ready")
                break

    @staticmethod
    def waitUntilNoChange(delay=None):
        vnc = ElinkSystem.vnc
        if delay is None:
            delay = 5000

        e = vnc.getEvent()

        vnc.setVncIdle(delay)
        while e.getIdCode() != 28:
            e = vnc.getEvent()
            # dbg(" |- event id: " + str(e.getIdCode()))

            if e.getIdCode() == 28:
                vnc.setVncIdle(0)
                return
            e = vnc.getEvent()
