import concurrent
import logging
import uuid

from apscheduler.schedulers.asyncio import AsyncIOScheduler

try:
    import asyncio
except ImportError:
    import trollius as asyncio

executor = concurrent.futures.ThreadPoolExecutor(max_workers=3)

logging.getLogger('apscheduler.executors').setLevel(logging.ERROR)
logging.getLogger('apscheduler.scheduler').setLevel('ERROR')
logging.getLogger('apscheduler.scheduler').propagate = False


async def func_test(func, *args):
    print("run func_test")
    loop = asyncio.get_event_loop()
    if len(args[0]) != 0:
        await loop.run_in_executor(executor, func, *args[0])
    else:
        await loop.run_in_executor(executor, func)


class TestRunner:
    def __init__(self) -> None:
        self.scheduler = AsyncIOScheduler()
        self.scheduler.start()
        super().__init__()

    def add_cron_task(self, func, *args, **kwargs):
        self.scheduler.add_job(func_test,
                               'cron',
                               args=[func, args], kwargs=kwargs)

    def add_test(self, func, *args, timeout=10, trigger='interval', **kwargs):
        if 'testid' in kwargs:
            testid = kwargs['testid']
        else:
            testid = str(uuid.uuid4())

        self.scheduler.add_job(func_test,
                               trigger,
                               seconds=timeout,
                               id=testid,
                               args=[func, args])

    def run(self):
        try:
            loop = asyncio.get_event_loop().run_forever()
        except KeyboardInterrupt:
            # Canceling pending tasks and stopping the loop
            asyncio.gather(*asyncio.Task.all_tasks()).cancel()
            # Stopping the loop
            loop.stop()
            # Received Ctrl+C
            loop.close()
