import sys
import os
import subprocess
import ElinkScript


class ScriptRunner:
    # def test_generate_hdd_img(self):
    #     generate_hdd_img("Win2012.iso", "Win2012.hdd2")
    ip = ""
    elinkviewer = "elinkviewer.exe"
    env = os.environ.copy()

    @staticmethod
    def setElinkViewer(viewer):
        print("VIewer: " + viewer)
        ScriptRunner.elinkviewer = viewer

    @staticmethod
    def setKvmIp(ip):
        ScriptRunner.ip = ip
        ScriptRunner.env["ELINKKVM_IP"] = ip

    @staticmethod
    def setScriptArgs(args):
        args_str = ",".join(str(a) for a in args)
        ScriptRunner.env['ELINKSCRIPT_ARGS'] = args_str

    @staticmethod
    def getScriptArg():
        env = os.environ.copy()
        return env['ELINKSCRIPT_ARGS'].split(",")

    @staticmethod
    def run(script_descriptor):
        if "args" in script_descriptor:
            ScriptRunner.setScriptArgs(script_descriptor["args"])

        script_dir = script_descriptor["dir"]
        p = script_dir + "/run.py"
        ScriptRunner.env["ELINKKVM_SCRIPTDIR"] = script_dir

        print("Path: " + p)

        subprocess.call([ScriptRunner.elinkviewer,
                         "-cons=" + p], env=ScriptRunner.env)
