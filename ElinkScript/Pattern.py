import ElinkScript
from ElinkScript.Debug import *
from ElinkScript.ElinkSystem import *
from ElinkScript.Utils import *


class Pattern(ElinkScript.Region):
    img_path = ""
    score = 0.8
    matched_score = 0.0
    prefix_path = ""

    @staticmethod
    def setImgPrefixPath(path):
        Pattern.prefix_path = path

    def __init__(self, img_path, score=0.8):
        self.img_path = img_path
        self.score = score

    def __eq__(self, other):
        if type(other) is str:
            return self.img_path == other
        elif type(other) is Pattern:
            return self.img_path == other.getFileName()

    def exists(self, timeout=3000):
        return self.waitImage(self, timeout)

    def click(self, delay=1):
        vnc = ElinkScript.ElinkSystem.vnc

        # if self.matched_score == 0.0:
        self.waitImage(self, delay, 1)

        l = self.getCenter()
        x = l.getX()
        y = l.getY()
        dbg("x: " + str(x) + " y: " + str(y))
        vnc.sendMouse(0, x, y)
        # right mouse click at 100,100	(or 0x40)
        vnc.sendMouse("LDOWN|CLICK", x, y)

    def setScore(self, new_score):
        self.score = new_score

    def getScore(self):
        return self.score

    def setMatchedScore(self, matched_score):
        self.matched_score = matched_score

    def getMatchedScore(self):
        return self.matched_score

    def getFileName(self):
        return normalize_path(Pattern.prefix_path + "/" + self.img_path)
