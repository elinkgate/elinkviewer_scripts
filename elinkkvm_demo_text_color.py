import time
from termcolor import cprint

def print_loading(text, succeed):
    for x in range (0,5):
        b = text + "." * x
        cprint (b, 'yellow', end="\r")
        time.sleep(0.5)

    # print("", end="\r")
    cprint (text + ".... ", 'green',end="")
    cprint(succeed, 'green')

cprint("eLinkKVM Automation Scripting Tools", 'cyan', attrs=['bold'])
print("")
print_loading("Connect to Device", "Connected")
print_loading("Scanning Virus", "No virus found")
print_loading("Backing Up Data", "Done")
print_loading("Reinstall Windows OS", "Windows installed")
print_loading("Restoring Data", "Done")
print("")
cprint("Actions completed!", 'green', attrs=['bold'])
