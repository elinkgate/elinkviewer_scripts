def testelink_getConnection():
    eLinkObj = elink.newConnection("10.42.0.2")
    info = eLinkObj.info()
    print("firmware version \t{}\neLinkKVM Name \t{}".format(info[0],info[1]))

    groupConn = elink.getConnection()
    print("group obj {} - type {}".format(groupConn,type(groupConn)))
    print("firmware version \t{}\neLinkKVM Name \t{}".format(groupConn[0].info()[0],groupConn[0].info()[1]))