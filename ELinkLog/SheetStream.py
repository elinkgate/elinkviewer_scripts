import json
import logging
import os

import gspread as gspread
from google.oauth2.credentials import Credentials
from oauth2client import client, tools
from oauth2client.file import Storage

cur_dir = os.path.dirname(__file__)

CREDENTIAL_FILE = os.path.join(cur_dir, "elinkgate.cre")
CLIENT_SECRETS_FILE = os.path.join(cur_dir, 'elinkgate_key.json')
SCOPES = ['https://spreadsheets.google.com/feeds',
          'https://www.googleapis.com/auth/drive']


class GoogleSheetStream(logging.Handler):
    def __init__(self, level=logging.NOTSET):
        super().__init__(level)
        self.store = Storage(CREDENTIAL_FILE)
        self.reset_authenticate()
        self.worksheet = self.get_worksheet()

    def reset_authenticate(self):
        self.gosheet = self.get_authenticated_service()

    def get_authenticated_service(self):
        if not os.path.isfile(CLIENT_SECRETS_FILE):
            print("Not found {}".format(CLIENT_SECRETS_FILE))
        credentials: Credentials = self.store.get()
        if not credentials or credentials.invalid:
            flow = client.flow_from_clientsecrets(CLIENT_SECRETS_FILE, SCOPES)
            credentials = tools.run_flow(flow, self.store)
        return gspread.authorize(credentials)

    def get_ipaddr(self, record):
        for key in record.keys():
            if 'ipaddr' in key:
                return record[key]
        return None

    def get_worksheet(self):
        logid = "TestResult"
        try:
            wks = self.gosheet.open("eLinkTest")
        except Exception as e:
            wks = self.gosheet.create('eLinkTest')
        try:
            worksheet = wks.worksheet(logid)
        except Exception as e:
            # if worksheet is None:
            wks.add_worksheet(logid, rows=1000, cols=30)
            worksheet = wks.worksheet(logid)
        return worksheet

    def update_record(self, index, record):
        row_values = self.worksheet.row_values(index)
        values = list(record.keys())
        if len(row_values) != len(values):  # add/remove field
            # remove the current => Insert the new
            self.worksheet.delete_row(index)
            self.worksheet.insert_row(list(record.values()), index=index)
        else:
            list_index = 0
            try:
                for key in values:
                    if 'info' in key:
                        info_index = values.index(key)
                        curInfo = int(row_values[info_index], 10)
                        row_values[info_index] = curInfo + record[key]
                        list_index = list_index + 1
                        continue
                    row_values[list_index] = record[key]
                    list_index = list_index + 1
                cell_list = self.worksheet.range(index, 1, index, len(row_values))
                index = 0
                for cell in cell_list:
                    cell.value = row_values[index]
                    index = index + 1
                    pass
                self.worksheet.update_cells(cell_list)
            except Exception as e:
                self.worksheet.delete_row(index)
                self.worksheet.insert_row(values, index=index)

    def emit(self, record):
        jsonrecord = self.format(record)
        jsondata: dict = json.loads(jsonrecord)
        devid = jsondata['devid']
        testid = jsondata['testid']
        try:
            row = self.find_log_index(devid, testid)
            if row is None:
                self.worksheet.append_row(list(jsondata.values()))
            else:
                self.update_record(row, jsondata)
        except Exception  as e:
            self.reset_authenticate()
            return

    def find_log_index(self, devid, testid):
        devid_cells = self.worksheet.findall(devid)
        if not len(devid_cells):
            return None
        testid_cells = self.worksheet.findall(testid)
        if not len(testid_cells):
            return None
        for dev in devid_cells:
            for test in testid_cells:
                if dev.row == test.row:
                    return dev.row
        return 0

    def get_record(self, worksheet):
        worksheet.find()
        json_obj = worksheet.get_all_records()
