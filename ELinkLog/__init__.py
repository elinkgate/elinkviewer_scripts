import sys

from ELinkLog.ELinkLog import ELinkLogFormatter
from ELinkLog.ELinkLog import ELinkLogCriticalFormatter
import logging
import logging.config

from pythonjsonlogger import jsonlogger
from structlog import configure, processors, stdlib, threadlocal

if not hasattr(sys, 'argv'):
    sys.argv = ['']


class CustomJsonFormatter(jsonlogger.JsonFormatter):

    def __init__(self, *args, **kwargs):
        self.sqliteDb = None
        super().__init__(*args, **kwargs)

    def set_sqliteDb(self, sqldb=None):
        self.sqliteDb = sqldb

    def parse(self):
        return jsonlogger.JsonFormatter.parse(self)

    def format(self, record):
        return super().format(record)


DEFAULT_CONFIGURE = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'json': {
            'format': '%(lineno)d %(message)s',
            'class': 'pythonjsonlogger.jsonlogger.JsonFormatter'
        },
        'jsontest': {
            'format': '%(devid)s %(testid)s %(asctime)s %(filename)d %(lineno)d %(message)s ',
            'class': 'ELinkLog.ELinkLogFormatter'
        },
        'criticallog': {
            'format': '%(devid)s %(testid)s %(asctime)s %(levelname)s %(filename)d %(lineno)d %(message)s ',
            'class': 'ELinkLog.ELinkLogCriticalFormatter'
        },
        'googlesheet': {
            'format': '%(devid)s %(testid)s %(asctime)s %(levelname)s %(filename)d %(lineno)d %(message)s ',
            'class': 'pythonjsonlogger.jsonlogger.JsonFormatter'
        }
    },
    'handlers': {
        'json': {
            'class': 'logging.StreamHandler',
            'formatter': 'json'
        },
        'jsonfile': {
            'class': 'logging.StreamHandler',
            'formatter': 'jsontest',
        },
        'tele': {
            'class': 'ELinkLog.TeleNotifier.TeleNotifyStream',
            'formatter': 'criticallog'
        },
        'critical': {
            'class': 'logging.StreamHandler',
            'formatter': 'criticallog',
        },
        # 'googlesheet': {
        #     'class': 'ELinkLog.SheetStream.GoogleSheetStream',
        #     'formatter': 'googlesheet',
        # }
    },
    'loggers': {
        'elinktest': {
            'handlers': ['jsonfile'],
            # 'handlers': ['jsonfile', 'googlesheet'],
            'level': logging.INFO
        },
        'elinkdebug': {
            'handlers': ['json'],
            'level': logging.INFO
        },
        'ecriticallog': {
            'handlers': ['critical', 'tele'],
            'level': logging.DEBUG
        },
        'telenotify': {
            'handlers': ['tele'],
            'level': logging.DEBUG
        },
        # 'googlesheet': {
        #     'handlers': ['googlesheet'],
        #     'level': logging.DEBUG
        # }

    }
}

logging.config.dictConfig(DEFAULT_CONFIGURE)
configure(
    context_class=threadlocal.wrap_dict(dict),
    logger_factory=stdlib.LoggerFactory(),
    wrapper_class=stdlib.BoundLogger,
    processors=[
        stdlib.filter_by_level,
        stdlib.add_logger_name,
        stdlib.add_log_level,
        stdlib.PositionalArgumentsFormatter(),
        processors.TimeStamper(fmt="iso"),
        processors.StackInfoRenderer(),
        processors.format_exc_info,
        processors.UnicodeDecoder(),
        stdlib.render_to_log_kwargs]
)

elinkdebug = logging.getLogger('elinkdebug')
elinkcritical = logging.getLogger('ecriticallog')
