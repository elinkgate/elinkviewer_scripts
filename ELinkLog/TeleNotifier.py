# !/usr/bin/env python
# -*- coding: utf-8 -*-

"""Simple Bot to reply to Telegram messages.
This program is dedicated to the public domain under the CC0 license.
This Bot uses the Updater class to handle the bot.
First, a few handler functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.
Usage:
Basic Echobot example, repeats messages.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""
import asyncio
import concurrent
import json
import os
import sqlite3

from telegram import Bot, Chat
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import logging

# Enable logging

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

elinkTeleUserDb = None
DEFAULT_DB = os.path.join(os.path.dirname(__file__), 'elinklog.sqlite3')
print(DEFAULT_DB)

eLinkGateBot: Bot = None
DebugLogGroup = -301512528
eLinkBotToken = '482729823:AAHdGNjzuX8IlU6rxzxEvi8wHHtxd3PlSV0'


class TeleRegisterUserDb:
    tele_reg_user_db = None

    def create_debug_db(self, dbfile=DEFAULT_DB):
        self.dbfile = dbfile
        self.debugdb = sqlite3.connect(dbfile)
        sql_create_tasks_table = """CREATE TABLE IF NOT EXISTS TeleRegisteredUser (
                                            id integer primary key ,
                                            type text,
                                            username text ,
                                            first_name text  ,
                                            last_name text 
                                        );"""
        cur = self.debugdb.cursor()
        cur.execute(sql_create_tasks_table)
        self.debugdb.commit()

    @classmethod
    def get_teleRegisteredUserDb(cls, dbfile=DEFAULT_DB):
        if TeleRegisterUserDb.tele_reg_user_db is None:
            TeleRegisterUserDb.tele_reg_user_db = TeleRegisterUserDb(dbfile)
            return TeleRegisterUserDb.tele_reg_user_db
        else:
            return TeleRegisterUserDb.tele_reg_user_db

    def __init__(self, dbfile):
        self.create_debug_db(dbfile)
        pass

    def get_userid(self, userid):
        debugdb = sqlite3.connect(self.dbfile)
        cur = debugdb.cursor()
        cur.execute('select * from TeleRegisteredUser where id=? ', (userid,))
        rows = cur.fetchall()
        debugdb.close()
        if len(rows) == 0:
            return None
        return rows

    def add_userid(self, user: Chat):
        """
                                            id integer primary key ,
                                            type text,
                                            username text NOT NULL,
                                            first_name text  NOT NULL,
                                            last_name text NOT NULL,
        :param user:
        :return:
        """
        curuser = self.get_userid(user.id)
        if curuser is not None:
            print("{} allready existed".format(user.username))
            eLinkGateBot.sendMessage(chat_id=user.id, text="{} {} already added".format(user.first_name,
                                                                                        user.last_name))
            return
        else:
            self.debugdb = sqlite3.connect(self.dbfile)
            cur = self.debugdb.cursor()
            print("insert new record")
            cur.execute(
                "insert into TeleRegisteredUser (id,type,username,first_name,last_name) values(?,?,?,?,?)",
                (user.id, user.type, str(user.username), user.first_name, user.last_name))
            self.debugdb.commit()
            self.debugdb.close()
            pass

    def get_all_userid(self):
        debugdb = sqlite3.connect(self.dbfile)
        cur = debugdb.cursor()
        cur.execute('select * from TeleRegisteredUser')
        rows = cur.fetchall()
        debugdb.close()
        userids = []
        for each in rows:
            userids.append(each[0])
        return userids


class TeleNotifyStream(logging.Handler):

    def __init__(self, level=logging.NOTSET):
        global elinkTeleUserDb
        global eLinkGateBot
        super().__init__(level=logging.NOTSET)
        self.userdb = TeleRegisterUserDb.get_teleRegisteredUserDb()
        self.elinkgateBot = Bot(token=eLinkBotToken)
        eLinkGateBot = self.elinkgateBot
        elinkTeleUserDb = self.userdb

    def get_ipaddr(self, record):
        for key in record.keys():
            if 'ipaddr' in key:
                return record[key]
        return None

    def send_log_file_to_registerd_user(self, ipaddr: str):
        from Utility.ELinkSSh import ELinkSSH
        curdir = os.path.dirname(__file__)
        try:
            DebugGetter = ELinkSSH(ipaddr)
        except Exception as exp:
            return
        dmesg_logfile = DebugGetter.get_dmesg(curdir)
        message_log = DebugGetter.get_message(curdir)
        elink_log = DebugGetter.get_elinklog(curdir)
        eLinkGateBot.sendDocument(chat_id=DebugLogGroup, document=open(dmesg_logfile, 'rb'))
        eLinkGateBot.sendDocument(chat_id=DebugLogGroup, document=open(message_log, 'rb'))
        eLinkGateBot.sendDocument(chat_id=DebugLogGroup, document=open(elink_log, 'rb'))

    def send_log(self, record):
        record = json.loads(record)
        if record['levelname'] == 'CRITICAL':
            ipaddr = self.get_ipaddr(record)
            if ipaddr is not None:
                self.send_log_file_to_registerd_user(ipaddr)

    def emit(self, record):
        json = self.format(record)
        for each in self.userdb.get_all_userid():
            eLinkGateBot.sendMessage(chat_id=each, text=json)
        eLinkGateBot.sendMessage(chat_id=DebugLogGroup, text=json)
        self.send_log(json)


from enum import IntEnum


class TestStatus(IntEnum):
    TEST_IDLE = 0
    TEST_RUNNING = 1
    TEST_STOP = 2
    TEST_ERROR = 3


class TestCase:

    def __init__(self, devid, testid, status=TestStatus.TEST_IDLE) -> None:
        self.testid = testid
        self.devid = devid
        self.status = status
        self.ipaddr = ''
        pass

    def set_ipaddr(self, ipaddr: str):
        self.ipaddr = ipaddr


class eLinkLogBotManager:
    bot = None
    elinkbot = None

    def __init__(self):
        self.testcases = []
        pass

    def add_test_case(self, testcase: TestCase):
        self.testcases.append(testcase)

    def remove_testcase(self, testcase):
        self.testcases.remove(testcase)

    def get_testcases(self):
        return self.testcases

    @classmethod
    def get_elinkbot(cls):
        if eLinkLogBotManager.elinkbot is None:
            eLinkLogBotManager.elinkbot = eLinkLogBotManager()
        return eLinkLogBotManager.elinkbot

    # Define a few command handlers. These usually take the two arguments bot and
    # update. Error handlers also receive the raised TelegramError object in error.
    @classmethod
    def start(cls, bot: Bot, update):
        """Send a message when the command /start is issued."""
        update.message.reply_text('Hi!')
        elinkTeleUserDb.add_userid(update.message.chat)

    def getlog(self, bot: Bot, update):
        # TODO parse message to get devid
        #  from deviceid => get test case
        # ssh to get dmes, elinklog

        pass

    @classmethod
    def help(cls, bot, update):
        """Send a message when the command /help is issued."""
        update.message.reply_text('Help!')

    @classmethod
    def echo(cls, bot: Bot, update):
        """Echo the user message."""
        update.message.reply_text(update.message.text)
        bot.sendMessage(chat_id=update.message.chat.id, text='hello')

    @classmethod
    def error(cls, bot, update, error):
        """Log Errors caused by Updates."""
        logger.warning('Update "%s" caused error "%s"', update, error)

    # @classmethod
    # def status(cls, bot, update):
    #     """Log Errors caused by Updates."""
    #     logger.warning('Update "%s" caused error "%s"', update)
    #     update.message.reply_text('current log return !')

    @classmethod
    def TeleNotifier_Runner(cls):
        """Start the bot."""
        # Create the EventHandler and pass it your bot's token.
        global eLinkGateBot
        if eLinkGateBot is None:
            eLinkGateBot = Bot(cls.eLinkBotToken)
        updater = Updater(bot=eLinkGateBot)

        # Get the dispatcher to register handlers
        dp = updater.dispatcher

        # on different commands - answer in Telegram
        dp.add_handler(CommandHandler("start", eLinkLogBotManager.start))
        dp.add_handler(CommandHandler("help", eLinkLogBotManager.help))

        # on noncommand i.e message - echo the message on Telegram
        dp.add_handler(MessageHandler(Filters.text, eLinkLogBotManager.echo))

        # log all errors
        dp.add_error_handler(eLinkLogBotManager.error)

        # Start the Bot
        updater.start_polling()

        # Run the bot until you press Ctrl-C or the process receives SIGINT,
        # SIGTERM or SIGABRT. This should be used most of the time, since
        # start_polling() is non-blocking and will stop the bot gracefully.
        updater.idle()

# if __name__ == '__main__':
#     main()
#
#
# if eLinkGateBot is None:
#     executor = concurrent.futures.ThreadPoolExecutor(max_workers=3)
#     loop = asyncio.get_event_loop()
#     entry = loop.run_in_executor(executor, TeleNotifier_Runner)
#     # TeleNotifier_Runner()
