import unittest
import elink
from Utility.ELinkUsb import *


class MyTestCase(unittest.TestCase):
    def test_elinkNewconnectionnonblocking(self):
        try:
            elinkobj = elink_newconnectionnonblocking(elink,
                                                      ELinkInfo(['10.42.0.2', 'kevin_test']))
            elink_setVncModeNonBlocking(elinkobj, VncMode.VNC_MODE_RGB)
        except Exception as timeout:
            print("timout error")

    def test_elink_nonblocking_setvnc(self):
        elinkobj = elink.newConnection('10.42.0.2')
        elinkusb = ELinkUsb(elinkobj)
        dev = elinkusb.swith_usb_mode(UsbId.ModeUsb_Msc)
        self.assertTrue(elinkusb.wait_for_dev_exist(dev),
                        "UsbMode {} not found Dev {}".
                        format(UsbId.ModeUsb_Msc.name,
                               UsbId.ModeUsb_Msc.value['devinfo']))


if __name__ == '__main__':
    unittest.main()
