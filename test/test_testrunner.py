import unittest
from ElinkScript.TestRunner import *


def hello_world(*args):
    print("hello world")


def hello_kevin(*args):
    kevin = args[0]
    company = args[1]
    code = args[2]
    print("hello {}".format(args[0]))


class test_testrunner(unittest.TestCase):

    def setUp(self):
        self.app = TestRunner()

    def test_something(self):
        self.app.add_test(hello_world)
        self.app.add_test(hello_kevin, "kevinelg", 'elinkgate', 100)
        self.app.run()


if __name__ == '__main__':
    unittest.main()
