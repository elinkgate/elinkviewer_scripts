import unittest
import elink
from Utility.ELinkUsb import *


class MyTestCase(unittest.TestCase):
    def test_querry_usb(self):
        elinkobj = ELinkUsb(elink)
        usbinfo = elinkobj.query_usbinfo()
        if len(usbinfo) != 0:
            for each in usbinfo:
                print("PID {} VID {}".format(each.PID, each.VID))
        self.assertTrue(len(usbinfo) != 0)

    def test_usb_mode(self):
        elinkobj = elink.newConnection('10.42.0.2')
        elinkusb = ELinkUsb(elinkobj)
        dev = elinkusb.swith_usb_mode(UsbId.ModeUsb_Msc)
        self.assertTrue(elinkusb.wait_for_dev_exist(dev),
                        "UsbMode {} not found Dev {}".
                        format(UsbId.ModeUsb_Msc.name,
                               UsbId.ModeUsb_Msc.value['devinfo']))


if __name__ == '__main__':
    unittest.main()
