import time
import unittest

from ELinkLog.ELinkLog import *
from ELinkLog.TeleNotifier import *
from Utility.ELinkSSh import ELinkSSH


def send_message():
    log = logging.getLogger('elinktest')
    while 1:
        time.sleep(3)
        log.critical("test_abc", extra={'devid': 'kevinelg',
                                        'testid': 'abc',
                                        'info': 10,
                                        'ipaddr': '10.42.0.2'})


class MyTestCase(unittest.TestCase):
    def setUp(self):
        self.parser = ELinkLogFormatter()
        pass

    def test_TeleLogger(self):
        executor = concurrent.futures.ThreadPoolExecutor(max_workers=3)
        loop = asyncio.get_event_loop()
        entry = loop.run_in_executor(executor, eLinkLogBotManager.TeleNotifier_Runner)
        entry = loop.run_in_executor(executor, send_message)
        try:
            loop = asyncio.get_event_loop().run_forever()
        except KeyboardInterrupt:
            # Canceling pending tasks and stopping the loop
            asyncio.gather(*asyncio.Task.all_tasks()).cancel()
            # Stopping the loop
            loop.stop()
            # Received Ctrl+C
            loop.close()

    def test_sendtelegramlog(self):
        criticallogger = logging.getLogger('telenotify')

        criticallogger.critical("test_abc", extra={'devid': 'kevinelg',
                                                   'testid': 'abc',
                                                   'info': 10,
                                                   'ipaddr': '10.42.0.2'})

    def test_adding_new_user(self):
        dbfile = 'test.sqlite3'
        testdb = TeleRegisterUserDb(dbfile)
        chatuser = Chat(id=10, type="Private", username='kevinelg', first_name='kevine',
                        last_name='truong')
        testdb.add_userid(chatuser)
        user = testdb.get_userid(chatuser.id)
        self.assertIsNotNone(user, "{}".format(user))
        user = testdb.get_userid(400)
        self.assertIsNone(user, "{} not nonde".format(user))
        all_user = testdb.get_all_userid()
        self.assertIsNotNone(all_user, "{} is None".format(all_user))
        self.assertEqual(len(all_user), 1)
        os.remove(dbfile)

    def test_basic_log(self):
        log = logging.getLogger('elinktest')
        debuglog = logging.getLogger('elinkdebug')
        log.debug("test_abc", extra={'devid': 'kevinelg', 'testid': 'abc', 'info': 10})
        log.critical("test_abc", extra={'devid': 'kevinelg', 'testid': 'abc', 'info': 10})
        now = '2018:10:21'
        while (1):
            time.sleep(3)
            log.critical("this is test abc",
                         extra={'info': 1, 'now': "{}".format(now)})

    def test_critical_log(self):
        log = logging.getLogger('ecriticallog')

        log.info("test_abc", extra={'devid': 'kevinelg', 'testid': 'abc', 'info': 10})

        log.critical("test_abc", extra={'devid': 'kevinelg', 'testid': 'abc', 'info': 10})

    def test_elinklog(self):
        log = logging.getLogger('elinktest')
        log.info("test_abc",
                 extra={'devid': 'Elink-KVM-43:55:a4',
                        'testid': 'test_elinklog',
                        'info_duration': 20,
                        'ipaddr': '10.42.0.2'})

    def test_parse_log(self):
        log_of_dev = self.parser.get_all_device_id('kevin truong')
        print(log_of_dev)
        pass

    def test_updateLogRecore(self):
        log = logging.getLogger('elinktest')
        log.debug("test_abc", extra={'info': "45"})
        self.parser.update_record()
        pass


class TestELinkGetLog(unittest.TestCase):
    def setUp(self):
        self.parser = ELinkSSH('10.42.0.2')
        pass

    def test_get_elinklog(self):
        outdir = os.path.dirname(__file__)
        self.parser.get_elinklog(outdir)
        self.assertTrue(True)

    def test_get_dmesg(self):
        outdir = os.path.dirname(__file__)
        self.parser.get_dmesg(outdir)
        self.assertTrue(True)

    def test_elinklog(self):
        outdir = os.path.dirname(__file__)
        self.parser.get_dmesg(outdir)
        self.assertTrue(True)


class TestGoogleSheetLogger(unittest.TestCase):
    def setUp(self):
        self.logger = logging.getLogger('googlesheet')

    def test_helloworld(self):
        while 1:
            self.logger.debug("hello world", extra={'devid': 'Elink-KVM-43:55:a4',
                                                    'testid': 'test_googlesheet_log',
                                                    'info_duration': 99,
                                                    'ipaddr': '10.42.0.2',
                                                    'abc': '10.42.0.2',
                                                    'info_count': 19
                                                    })

            time.sleep(2)
            self.logger.debug("hello world", extra={'devid': 'Elink-KVM-43:55:a2',
                                                    'testid': 'test_googlesheet_test',
                                                    'info_duration': 40,
                                                    'ipaddr': '10.42.0.2',
                                                    'test': 'test'})

            time.sleep(2)
            self.logger.debug("hello world", extra={'devid': 'Elink-KVM-43:55:a9',
                                                    'testid': 'test_googlesheet_test',
                                                    'info_count': 40,
                                                    'ipaddr': '10.42.0.2',
                                                    'test': 'abc xyz',
                                                    'newfield': 'abc xyz',
                                                    'new field 1': 'abc xyz1'})

            time.sleep(2)


if __name__ == '__main__':
    unittest.main()
