import serial
from Utility.ELinkUtils import *


class ELinkSerial:
    def __init__(self, elink, port):
        self.elink = elink
        self.comport = serial.Serial(port, 115200)
        print("ELinkSerial connected to port {}".format(port))
        pass

    def __del__(self):
        self.comport.close()

    def enable_elinkserialmode(self):
        self.elink.setVncMode(VncMode.VNC_MODE_SERIAL)
        pass

    def write(self, serialdata):
        self.comport.write(serialdata)
        pass
