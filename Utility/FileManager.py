import json
import logging
import os
from enum import IntEnum
from os import path

from ElinkScript import ELinkInfo
from Utility.ELinkUtils import timeout


class FileType(IntEnum):
    TYPE_FILE = 0
    TYPE_DIR = 1


class Entry:
    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)

    def __init__(self, entryObj: list):
        self.name = entryObj[0]
        self.type = entryObj[1]
        self.size = entryObj[2]
        self.fullpath = None

    def show(self):
        return print(self.toJSON())

    def setFullPath(self, fullPath: str):
        self.fullpath = fullPath


class FileManager:
    def __init__(self, elinkObj, devinfo):
        self._elink = elinkObj
        self.devinfo: ELinkInfo = devinfo
        self.logger = logging.getLogger('FileManager')

    def __isDirectoryType__(self, curpath: str):
        entry = self.find_entry(curpath)
        if entry.type == FileType.TYPE_DIR:
            return True
        return False

    def __isFileType__(self, curpath: str):
        entry = self.find_entry(curpath)
        if entry.type == FileType.TYPE_FILE:
            return True
        return False

    def entry_list(self, entry=None):
        if entry is None:
            self.logger.info("entry {} ".format(entry))
            entryList = self._elink.remoteFileList()
        else:
            self.logger.info("entry {} ".format(entry))
            entryList = self._elink.remoteFileList(entry)
        entries = []
        if len(entryList) == 0:
            return None
        for obj in entryList:
            element = Entry(obj)
            if entry is None:
                element.setFullPath(element.name)
            else:
                element.setFullPath(entry + "/" + element.name)
            # element.show()
            entries.append(element)
        return entries

    def get_entry_info(self, entryPath=None):
        """

        :rtype: object
        """
        if entryPath is None:
            self.logger.info("entry {} ".format(entryPath))
            entryList = self._elink.remoteFileList()
        else:
            self.logger.info("entry {} ".format(entryPath))
            entryList = self._elink.remoteFileList(entryPath)
        if len(entryList) == 0:
            return None
        else:
            entry = Entry(Entry(entryList[0]))
            return entry

    def find_entry(self, filename: str, entrypoint=None):
        """
        :return entry file path or None
        """
        self.logger.info("Entry {} Searching ... ".format(entrypoint))
        entrylist = self.entry_list(entrypoint)
        if entrylist is None:
            return None
        for entry in entrylist:
            self.logger.info("Entry: {:20} type: {:5}".format(entry.name, entry.type))
            if entry.name == filename:
                entrypoint = entrypoint.replace("/", "")
                entry.setFullPath("/{}/{}".format(entrypoint, entry.name))
                return entry
            else:
                if entry.type == FileType.TYPE_DIR:
                    if entrypoint is None:
                        return self.find_entry(entrypoint="/{}".format(entry.name), filename=filename)
                    else:
                        entrypoint = entrypoint.replace("/", "")
                        entry.setFullPath("/{}/{}".format(entrypoint, entry.name))
                        return self.find_entry(entrypoint=entry.fullpath, filename=filename)
            pass
        return None

    # @timeout(5)
    def remove_dir(self, entrypoint=None, recursively=False):
        """
        :return entry file path or None
        """
        self.logger.info("Entry {} Searching ... ".format(entrypoint))
        entrylist = self.entry_list(entrypoint)
        if entrylist is None:
            return None
        for entry in entrylist:
            self.logger.info(entry.toJSON())
            if entry.type == FileType.TYPE_DIR:
                if entrypoint is None:
                    self.remove_dir(entrypoint="/{}/".format(entry.name), recursively=recursively)
                else:
                    # entrypoint = entrypoint.replace("/", "")
                    entry.setFullPath("{}/{}".format(entrypoint, entry.name))
                    self.remove_dir(entrypoint=entry.fullpath, recursively=recursively)
            if entrypoint is not None and entry.type != FileType.TYPE_DIR:
                self.logger.info("remote entry {}".format(entry.toJSON()))
                self.remove_file(entry)
        return None

    @classmethod
    def convert_viewerpath_localpath(cls, viewerpath: str):
        if 'A' in viewerpath:
            dir, filename = os.path.split(viewerpath)
            return r'//mnt' + r'//{}'.format(filename)
        else:
            return None

    def upload_file(self, srcfile: str, des: str, verify=False):
        """

        :param srcfile:
        :param des: directory
        """
        dir, filename = os.path.split(srcfile)
        if path.isfile(srcfile) is False:
            raise Exception("not found file {}".format(srcfile))
        self._elink.remoteFileUpload(srcfile, des)
        viewerpath = des + '{}'.format(filename)
        if verify:
            self.ssh_verify_md5(srcfile, viewerpath)
        return viewerpath

    def get_md5_ssh(self, viewerpath):
        remote_path = FileManager.convert_viewerpath_localpath(viewerpath)
        from Utility.ELinkSSh import ELinkSSH
        elinkssh = ELinkSSH(self.devinfo.get_dev_ip())
        remote_md5 = elinkssh.get_md5(remote_path)
        return remote_md5

    def ssh_verify_md5(self, srcfile, viewerpath):
        from Utility.ELinkUtils import get_md5_checksum
        local_md5 = get_md5_checksum(srcfile)
        remote_path = FileManager.convert_viewerpath_localpath(viewerpath)
        from Utility.ELinkSSh import ELinkSSH
        elinkssh = ELinkSSH(self.devinfo.get_dev_ip())
        remote_md5 = elinkssh.get_md5(remote_path)
        if local_md5 != remote_md5:
            raise Exception("expected: {}, actual {} File Transfer error".
                            format(local_md5, remote_md5))
        print("Matching md5")
        return True
        pass

    def list_file(self, des: str):
        entries = self.entry_list(des)
        if entries is None:
            return None
        return entries

    def list_dir(self, entrypoint=None, recursively=False):
        """
        :return entry file path or None
        """
        listentry = []
        self.logger.info("Entry {} Searching ... ".format(entrypoint))
        entrylist = self.entry_list(entrypoint)
        if entrylist is None:
            return listentry
        for entry in entrylist:
            if entry.type == FileType.TYPE_DIR:
                if entrypoint is None:
                    return listentry.append(self.list_dir(entrypoint="/{}".format(entry.name), recursively=recursively))
                else:
                    entrypoint = entrypoint.replace("/", "")
                    entry.setFullPath("/{}/{}".format(entrypoint, entry.name))
                    return listentry.append(self.list_dir(entrypoint=entry.fullpath, recursively=recursively))
            else:
                listentry.append(entry)
        pass

    def remove_file(self, entry: Entry):
        if entry.fullpath is None:
            return
        # if self.get_entry_info(entry.fullpath) is None:
        #     return
        self._elink.remoteFileDelete(entry.fullpath)
        pass

    def rename_entry(self, entry: Entry, newname: str):
        myEntry = self.find_entry(entry.name, entrypoint=entry.fullpath)
        if myEntry is None:
            return
        dirname = path.dirname(newname)
        dir = self.get_entry_info(dirname)
        if dir is None:
            return
        if dir.type != FileType.TYPE_DIR:
            return
        self._elink.remoteRenameFile(entry.fullpath, newname)
        pass

    def get_md5(self, filepath: str):
        self.logger.info("not implemented yet")
        return None


import unittest
import elink


class test_elinkfilemanager(unittest.TestCase):
    def setUp(self) -> None:
        elinkobj = elink.newConnection('10.42.0.2')
        self.filemnger = FileManager(elinkobj,
                                     ELinkInfo(['10.42.0.2', 'TEST_ELINK_FILEMNGER']))
        super().setUp()

    def test_uploadfile_verify(self):
        testfile = os.path.realpath(__file__)
        viewerpath = self.filemnger.upload_file(testfile,
                                                r'/A:/',
                                                verify=True)
        self.assertTrue(r'/A:/{}'.format(__file__), viewerpath)
        pass

    def test_upload_and_verify(self):
        testfile = os.path.realpath(__file__)
        viewerpath = self.filemnger.upload_file(testfile,
                                                r'/A:/',
                                                verify=True)
        self.filemnger.ssh_verify_md5(testfile, viewerpath)
