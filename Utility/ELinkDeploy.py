import time

import elink

from ELinkLog import elinkdebug
from Utility.ELinkSSh import *
from Utility.ELinkUtils import *

CurDir = os.path.dirname(os.path.realpath(__file__))
ResourceDir = os.path.join(CurDir, r'..\Resource')

ResourceDirMap = {
    'Firmware': os.path.join(ResourceDir, 'Firmware'),
    'Images': os.path.join(ResourceDir, 'Images')
}

FirmwareVersionMap = {
    'V1.3.1.4': os.path.join(ResourceDirMap['Firmware'], 'V1.3.1.4'),
    'V1.3.1.5': os.path.join(ResourceDirMap['Firmware'], 'V1.3.1.5'),
}


class ElinkFirmware:
    ResourceDir = os.path.join(CurDir, r'../Resource/Firmware')

    def __init__(self, elinkobj, devinf, fw_version: str):
        from Utility.FileManager import FileManager
        filemnger = FileManager(elinkobj, devinf)
        self.firmwareDir = FirmwareVersionMap[fw_version]
        self.filemnger: FileManager = filemnger
        self.elinkobj = elinkobj

    def monitor_fwupgrade(self):
        while True:
            ev = self.elinkobj.getEvent()
            if ev:
                if ev.getIdCode() == ElinkEvent.EVT_SYS_FW_UPDATE_PROGRESS_EVT.value:
                    value = ev.getData("upgrade_percent")
                    if value < 0:
                        raise Exception("Upgrade firmware fail")
                    elif value == 100:
                        elinkdebug.info("Upgrade success Progress : {}%".format(value))
                        return
                    else:
                        elinkdebug.info("Upgrade progress: {}%".format(value))

    def upgrade_firmware_file(self, remotepath: str):
        filename = os.path.basename(remotepath)
        entry = self.filemnger.find_entry(filename)
        if entry is None:
            raise Exception("Not found {} in ELinkKVM".format(remotepath))
        self.elinkobj.systemUpgrade(entry.fullpath)
        self.monitor_fwupgrade()
        self.filemnger.remove_file(entry)

    def upgrade_firware_local_file(self, localfile):
        remotefilepath = self.filemnger.upload_file(localfile, "/A:/")
        self.upgrade_firmware_file(remotefilepath)

    def upgrade_firmware(self):
        try:
            for localfwfile in os.listdir(self.firmwareDir):
                fullfilepath = os.path.join(self.firmwareDir, localfwfile)
                remotefilepath = self.filemnger.upload_file(fullfilepath, "/A:/")
                self.upgrade_firmware_file(remotefilepath)
        except Exception as e:
            raise e


class ELinkDeploy:
    ResourceDir = os.path.join(CurDir, r'../Resource')

    def __init__(self, dev: ELinkInfo, usrname='admin', password='admin', fwversion=None):
        self.devinfo = dev
        self.fwversion = fwversion
        self.username = usrname
        self.password = password
        pass

    def get_new_connection(self):
        return elink.newConnection(self.devinfo.get_dev_ip(), self.username, self.password)

    def __del__(self):
        # self.elink.close()
        pass

    def reset_dev(self):
        from Utility.FileManager import FileManager
        try:
            elinkobj = elink.newConnection(self.devinfo.get_dev_ip(), self.username, self.password)
            filemnger = FileManager(elinkobj, self.devinfo)
            elinkssh = ELinkSSH(self.devinfo.get_dev_ip())
            elinkssh.clear_trial()
            filemnger.remove_dir(recursively=True)
            elinkdebug.info("reset device done")
        except Exception as exp:
            print("error on reset_dev".format(exp))
            raise exp

    def upload_factory_images(self):
        from Utility.FileManager import FileManager
        elinkobj = elink.newConnection(self.devinfo.get_dev_ip(), self.username, self.password)
        filemnger = FileManager(elinkobj, self.devinfo)
        for eachfile in os.listdir(ResourceDirMap['Images']):
            localfile = os.path.join(ResourceDirMap['Images'], eachfile)
            filemnger.upload_file(localfile, '/A:/')
        pass
        sleep(1)

    def upgrade_firmware(self, version):
        if version in FirmwareVersionMap:
            for eachfile in os.listdir(FirmwareVersionMap[version]):
                firmwarefile = os.path.join(FirmwareVersionMap[version], eachfile)
                elinkobj = self.get_new_connection()
                fwupgrader = ElinkFirmware(elinkobj, self.devinfo, self.fwversion)
                fwupgrader.upgrade_firware_local_file(firmwarefile)
                print("firwmare upgrade {}".format(firmwarefile))
        else:
            print("not founf {} in firmware dir".format(version))
            pass

    def deploy(self):
        try:
            self.reset_dev()
            self.upload_factory_images()
            if self.fwversion is not None:
                self.upgrade_firmware(self.fwversion)
        except Exception as e:
            print("error exception : {}".format(e))


import unittest


class TestELinkDeploy(unittest.TestCase):
    def setUp(self):
        self.elinkobj = elink.newConnection('10.42.0.2', 'admin', 'admin')
        self.deployer = ELinkDeploy(self.elinkobj,
                                    ELinkInfo(['10.42.0.2', 'Kevin test device']),
                                    'V1.3.1.4')

    def tearDown(self):
        time.sleep(1)

    def test_reset(self):
        self.deployer.reset_dev()

    def test_upgrade_firmware(self):
        self.deployer.upgrade_firmware('V1.3.1.4')

    def test_upload_factory_images(self):
        self.deployer.upload_factory_images()

    def test_deploy(self):
        self.deployer.deploy()


class TestElinkFirmware(unittest.TestCase):
    def setUp(self):
        self.elinkobj = elink.newConnection('10.42.0.2', 'admin', 'admin')
        devinfo = ELinkInfo(['10.42.0.2', 'kevinelg'])
        from Utility.FileManager import FileManager
        self.filemnger = FileManager(self.elinkobj, devinfo)
        self.upgrader = ElinkFirmware(self.elinkobj, self.filemnger, 'V1.3.1.4')

    def deploy_new_elinkkvm(self):
        self.elinkssh = ELinkSSH("10.42.0.2")
        elinkkvm_exe = r'/usr/bin/elink-kvm'
        local_elinkkvm_exe = os.path.join(ResourceDir, 'elink-kvm')
        remove_elink = r'rm -rf {}'.format(elinkkvm_exe)
        self.elinkssh.ssh_run_command(remove_elink)
        self.elinkssh.put_file(local_elinkkvm_exe, elinkkvm_exe)
        chmod_elink = r'chmod 777 {}'.format(elinkkvm_exe)
        self.elinkssh.ssh_run_command(chmod_elink)
        kill_elinkkvm = 'kill elink-kvm'
        self.elinkssh.ssh_run_command(kill_elinkkvm)

    def test_firwmare_upgrade(self):
        self.upgrader.upgrade_firmware()
        sleep(5)
        wait_elinkkvm_ready('10.42.0.2')

    pass
