import time

from ElinkScript import ELinkInfo
from Utility.ELinkSSh import *
import os
import paramiko
from scp import SCPClient


class ELinkUsbHostMsc:
    def __init__(self, elinkobj, devinf: ELinkInfo):
        self.elinkobj = elinkobj
        self.elinkssh = ELinkSSH(devinf.ipAddr)
        pass

    def reconfigure_usbhost(self):
        stdin, stdout, stderr = self.elinkssh.ssh.exec_command('ls -al /dev/sda*')
        print(stdout.read().decode("utf-8"))
        self.elinkssh.ssh.exec_command('umount /mnt')
        self.elinkssh.ssh.exec_command('mount /dev/sda1 /mnt')
        time.sleep(1)
        stdin, stdout, stderr = self.elinkssh.ssh.exec_command('mount')
        print(stdout.read().decode("utf-8"))


import unittest


class TestELinkUsbHost(unittest.TestCase):
    def test_reconfigure_usbhost(self):
        elinkusbhost = ELinkUsbHostMsc(elinkobj=None, devinf=ELinkInfo(['10.42.0.2',
                                                                        'Kevin test device']))
        elinkusbhost.reconfigure_usbhost()
