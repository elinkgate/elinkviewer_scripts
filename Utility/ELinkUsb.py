import re

from Utility.FileManager import *
from Utility.ELinkUtils import *
from enum import IntEnum


class ELinkUsbMode(IntEnum):
    ModeUsb_Key_Mouse_Msc = UsbMode.USB_MODE_MOUSE | UsbMode.USB_MODE_KEY | UsbMode.USB_MODE_MSC
    ModeUsb_Key_MouseAbs_Msc = UsbMode.USB_MODE_MOUSE_ABS | UsbMode.USB_MODE_KEY | UsbMode.USB_MODE_MSC
    ModeUsb_Key_Msc = UsbMode.USB_MODE_KEY | UsbMode.USB_MODE_MSC
    ModeUsb_Mouse_Msc = UsbMode.USB_MODE_MOUSE | UsbMode.USB_MODE_MSC
    ModeUsb_MouseAbs_Msc = UsbMode.USB_MODE_MOUSE_ABS | UsbMode.USB_MODE_MSC
    ModeUsb_Key = UsbMode.USB_MODE_KEY
    ModeUsb_Mouse = UsbMode.USB_MODE_MOUSE
    ModeUsb_Msc = UsbMode.USB_MODE_MSC


class DevInfo:
    def __init__(self, vIdpId: []):
        if vIdpId is not None:
            self.VID = int(vIdpId[0], 16)
            self.PID = int(vIdpId[1], 16)

    def __eq__(self, o: object) -> bool:
        if self.PID == o.PID and self.VID == o.VID:
            return True
        return False


class ELinkUsbInfo:
    def __init__(self, devinfo: dict):
        if devinfo['master'] is not None:
            self.master = DevInfo(devinfo['master'])
        else:
            self.master = None
        if devinfo['slave'] is not None:
            self.slave = DevInfo(devinfo['slave'])
        else:
            self.slave = None


class UsbId(Enum):
    ModeUsb_Key_Mouse_Msc = {'devinfo': {"master": ['04cc', '2b21'],
                                         "slave": ['1fc9', '000c']},
                             'mode': ELinkUsbMode.ModeUsb_Key_Mouse_Msc}

    ModeUsb_Key_MouseAbs_Msc = {'devinfo': {"master": ['04cc', '2b21'],
                                            "slave": ['1fc9', '000c']},
                                'mode': ELinkUsbMode.ModeUsb_Key_MouseAbs_Msc}

    ModeUsb_Key_Msc = {'devinfo': {"master": ['04cc', '2b21'],
                                   "slave": ['1fc9', '000c']},
                       'mode': ELinkUsbMode.ModeUsb_Key_Msc}

    ModeUsb_Mouse_Msc = {'devinfo': {"master": ['04cc', '2b21'],
                                     "slave": ['1fc9', '000c']},
                         'mode': ELinkUsbMode.ModeUsb_Mouse_Msc}

    ModeUsb_MouseAbs_Msc = {'devinfo': {"master": ['04cc', '2b21'],
                                        "slave": ['1fc9', '000c']},
                            'mode': ELinkUsbMode.ModeUsb_MouseAbs_Msc}
    ModeUsb_Key = {'devinfo': {"master": ['04cc', '2b21'],
                               "slave": ['1fc9', '000c']},
                   'mode': ELinkUsbMode.ModeUsb_Key}

    ModeUsb_Mouse = {'devinfo': {"master": ['04cc', '2b21'],
                                 "slave": ['1fc9', '000c']},
                     'mode': ELinkUsbMode.ModeUsb_Mouse}
    ModeUsb_Msc = {'devinfo': {"master": ['04cc', '2b21'],
                               "slave": None},
                   'mode': ELinkUsbMode.ModeUsb_Msc}


class ELinkUsb:
    usbmode_image = "A:\\testusb.hdd2"
    testimage = os.path.join(os.path.dirname(__file__), '..\\ELinkImage\\testusb.hdd2')

    def __init__(self, elinkobj):
        self.elink = elinkobj
        self.filemnger = FileManager(elinkobj)
        file = self.filemnger.find_entry('testusb.hdd2')
        if file is None:
            if os.path.isfile(ELinkUsb.testimage):
                self.filemnger.upload_file(ELinkUsb.testimage, "/A:")
        pass

    def check_file_existed(self):
        pass

    def swith_usb_mode(self, usbmode: UsbId):
        self.elink.setUsbMode(usbmode.value['mode'], 0, [ELinkUsb.usbmode_image])
        return ELinkUsbInfo(usbmode.value['devinfo'])

    def query_usbinfo(self):
        import win32com.client
        wmi = win32com.client.GetObject("winmgmts:")
        usblst = []
        for usb in wmi.InstancesOf("Win32_USBHub"):
            usbinfo = usb.DeviceID
            if 'VID' in usbinfo and 'PID' in usbinfo:
                m = re.match(r'(.*)\\(.*)&(.*)\\(.*)', usbinfo, re.M | re.I)
                group2 = m.group(2)[-4:]
                group3 = m.group(3)[-4:]
                usblst.append(DevInfo([group2, group3]))
        return usblst

    def wait_for_dev(self, devinf: DevInfo, timeout=10):
        cout = 0
        while True:
            usblst = self.query_usbinfo()
            for each_dev in usblst:
                if each_dev == devinf:
                    return True
            sleep(1)
            cout = cout + 1
            if cout > timeout:
                return False

    def wait_for_dev_exist(self, elinkinfo: ELinkUsbInfo, timeout=10):
        master_dev = elinkinfo.master
        slave_dev = elinkinfo.slave
        if master_dev is not None:
            if not self.wait_for_dev(master_dev):
                return False
        if slave_dev is not None:
            if not self.wait_for_dev(slave_dev):
                return False
        return True
