import os

import paramiko
from scp import SCPClient


class ELinkSSH:
    def __init__(self, ipaddr):
        pkeyfile = os.path.join(os.path.dirname(__file__),
                                'elink_kvm_dev_ssh')
        self.ipaddr = ipaddr
        self.ssh = paramiko.SSHClient()
        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.ssh.connect(ipaddr,
                         username='root',
                         password='',
                         key_filename=pkeyfile,
                         passphrase='elgdev')

    def __del__(self):
        self.ssh.close()

    def get_message(self, outdir):
        dmesgfile = os.path.join(outdir, 'message.log')
        with SCPClient(self.ssh.get_transport()) as scp:
            scp.get('/var/log/messages', dmesgfile)
        return dmesgfile

    def get_dmesg(self, outdir):
        dmesgfile = os.path.join(outdir, 'dmesg.log')
        command = 'dmesg'
        self.ssh.invoke_shell()
        stdin, stdout, stderr = self.ssh.exec_command(command)
        with open(dmesgfile, 'w') as file:
            file.write(str(stdout.read().decode("utf-8")))
        return dmesgfile

    def get_elinklog(self, outdir):
        elinklog = os.path.join(outdir, 'S20elinkd.log')
        with SCPClient(self.ssh.get_transport()) as scp:
            scp.get('/var/log/S20elinkd.log', elinklog)
        return elinklog

    def get_md5(self, remotefile: str):
        """

        :param remotefile:  absPath of remote file
        :return:
        """
        self.ssh.invoke_shell()
        command = r'md5sum {}'.format(remotefile)
        stdin, stdout, stderr = self.ssh.exec_command(command)
        md5file = str(stdout.read().decode("utf-8")).split()
        if len(md5file):
            return md5file[0]
        return None

    def put_file(self, localfile: str, remotefile: str):
        with SCPClient(self.ssh.get_transport()) as scp:
            scp.put(localfile, remote_path=remotefile)
        return remotefile

    def ssh_run_command(self, command):
        self.ssh.invoke_shell()
        stdin, stdout, stderr = self.ssh.exec_command(command)
        return stdin, stdout, stderr

    def check_file_exists(self, filepath):
        sftp = self.ssh.open_sftp()
        try:
            print(sftp.stat(filepath))
            print('file exists')
            return True
        except IOError:
            print('copying file')
            return False

    def clear_trial(self):
        trialfile = r"/etc/elink/trial"
        command = r'rm -rf {}'.format(trialfile)
        # if self.check_file_exists(trialfile):
        self.ssh_run_command(command)
        # self.ssh_run_command('reboot')


import unittest
import hashlib


class Test_ELinkSSH(unittest.TestCase):

    def setUp(self) -> None:
        super().setUp()
        self.elinkssh = ELinkSSH('10.42.0.251')

    def test_getmd5(self):
        from Utility.ELinkUtils import get_md5_checksum
        testfile = os.path.realpath(__file__)
        self.elinkssh.put_file(testfile, '~/')
        md5expected = get_md5_checksum(testfile)
        md5_value = self.elinkssh.get_md5(r'~/{}'.format(os.path.basename(__file__)))
        self.assertEqual(md5_value, md5expected)


import elink
from ElinkScript import ELinkInfo


class test_remotefile(unittest.TestCase):
    def setUp(self):
        from Utility.FileManager import FileManager
        ipaddr = '10.42.0.251'
        self.elinkobj = elink.newConnection(ipaddr)
        self.filemnger = FileManager(self.elinkobj, ELinkInfo)
        self.elinkssh = ELinkSSH(ipaddr)
        pass

    def test_upload_verify(self):
        from Utility.ELinkUtils import get_md5_checksum
        from Utility.FileManager import FileManager
        testfile = os.path.realpath(__file__)
        viewerpath = self.filemnger.upload_file(testfile, r'\A:\\')
        uploadedfile = FileManager.convert_viewerpath_localpath(viewerpath)
        ssh_filemd5 = self.elinkssh.get_md5(uploadedfile)
        local_filemd5 = get_md5_checksum(testfile)
        self.assertTrue(ssh_filemd5, local_filemd5)

    def test_clear_trial(self):
        self.elinkssh.clear_trial()
