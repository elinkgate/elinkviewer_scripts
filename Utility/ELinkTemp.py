import json
import time
from enum import IntEnum
from threading import Thread
from typing import Optional

import ELinkLog
from ELinkLog import elinkdebug
import elink
from Utility.ELinkUtils import *


class ProbeTemp(IntEnum):
    TEMP_MASTER = 0
    TEMP_SLAVE = 1


class TempData:

    def __init__(self):
        self.curTemp = 0
        self.avgTemp = 0
        self.maxTemp = 0
        self.minTemp = 0
        self.elapseTime = 0
        self.startTime = int(time.time())

        # self.temp = []

    def process_temp(self, temp):
        # self.temp.append(temp)
        if self.minTemp == 0:
            self.minTemp = temp
        if temp > self.maxTemp:
            self.maxTemp = temp

        if temp < self.minTemp:
            self.minTemp = temp
        if self.curTemp == 0:
            self.avgTemp = temp

        # if len(self.temp) == 1:
        #     self.avgTemp = temp
        self.avgTemp = (self.avgTemp + temp) / 2
        self.curTemp = temp
        self.elapseTime = int(time.time() - self.startTime)
        print(self.toJSON())

        pass

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)


class ELinkTemp(Thread):
    def __init__(self, elinkobj):
        super().__init__()
        self.isStopped = False
        self.finished = False
        self.elinkobj = elinkobj
        self.masterTemp = TempData()
        self.slaveTemp = TempData()

    def store_temp_data(self, protemp: ProbeTemp, temp: float):
        if protemp == ProbeTemp.TEMP_MASTER:
            self.masterTemp.process_temp(temp)
        elif protemp == ProbeTemp.TEMP_SLAVE:
            self.slaveTemp.process_temp(temp)
        else:
            raise Exception('not valid {} probe'.format(protemp.name))

    def get_temp(self, protemp: ProbeTemp):
        self.finished = False
        self.elinkobj.probeTemp(protemp.value)
        print("Get temp {}".format(protemp.name))
        while True:
            if self.finished:
                break
            else:
                ev = self.elinkobj.getEvent()
                print("get event {}".format(ev))
                if ev:
                    idcode = ev.getIdCode()
                    master_id_code = ElinkEvent.EVT_PROBE_TEMP_MASTER.value
                    if idcode == master_id_code:
                        value = ev.getData("temp")
                        elinkdebug.info("template of master: {}".format(value))
                        self.masterTemp.process_temp(value)
                        return value
                    elif idcode == ElinkEvent.EVT_PROBE_TEMP_SLAVE.value:
                        value = ev.getData('temp')
                        elinkdebug.info("template of slave: {}".format(value))
                        self.slaveTemp.process_temp(value)
                        return value
                    else:
                        print(idcode)
                        pass
                sleep(1)

    def get_all_temp(self):
        self.get_temp(ProbeTemp.TEMP_MASTER)
        self.get_temp(ProbeTemp.TEMP_SLAVE)

    def finish(self):
        self.isStopped = True
        pass

    def run(self):
        while True:
            elinkdebug.info('process monitor temp')
            if self.finished:
                elinkdebug.info('exit monitor temp')
                break
            self.get_temp(ProbeTemp.TEMP_MASTER)
            self.get_temp(ProbeTemp.TEMP_SLAVE)
            sleep(5)

    def join(self, timeout: Optional[float] = ...):
        self.finished = True


import unittest


class Test_template(unittest.TestCase):
    def setUp(self):
        self.elinkobj = elink.newConnection('10.42.0.2', 'admin', 'admin')
        self.tempmonitor = ELinkTemp(self.elinkobj)
        sleep(1)
        pass

    def test_get_temp(self):
        while True:
            self.tempmonitor.get_temp(ProbeTemp.TEMP_MASTER)
            self.tempmonitor.get_temp(ProbeTemp.TEMP_SLAVE)
            sleep(1)
        pass

    def test_run_monitor_thread_start(self):
        self.tempmonitor.start()
        sleep(10)
        self.tempmonitor.join()
