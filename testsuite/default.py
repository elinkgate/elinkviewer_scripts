test_cases = [
    {
        "key": "setup_win2012",
        "dir": "ScriptWork/test_cases/setup_win2012",
        "args": ["1", "2"]
    },

    {
        "key": "setup_win10",
        "dir": "ScriptWork/test_cases/setup_win10"
    },

    {
        "key": "setup_win2012_ibmx3530",
        "dir": "ScriptWork/test_cases/setup_win2012_ibmx3530",
        "args": ["1", "2"]
    },

    {
        "key": "setup_win8",
        "dir": "ScriptWork/test_cases/setup_win8"
    },

    {
        "key": "test_duration",
        "dir": "ScriptWork/test_cases/test_duration"
    },

    {
        "key": "flashing_color_stress_test",
        "dir": "ScriptWork/test_cases/flashing_color_stress_test"
    },
    {
        "key": "test_on_off",
        "dir": "ScriptWork/test_cases/test_on_off"
    },
    {
        "key": "test_emmc",
        "dir": "ScriptWork/test_cases/test_emmc"
    },
    {
        "key": "test_serial ",
        "dir": "ScriptWork/test_cases/test_serial"
    },
    {
        "key": "test_on_off_debug",
        "dir": "ScriptWork/test_cases/test_on_off_debug"
    },

]
