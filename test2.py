def main():
	#vnc = elink.newConnection("10.42.0.2")
	#vnc.setKeyIdle(10)
	#vnc.setVncIdle(2000) #set trigger event if idle for 2 second
	#print("set USB Keyboard")
	#vnc.setUsbMode("USB_MODE_KEY|USB_MODE_VNC_HID",0,["B:\syslinux.hddx"])
	#vnc.setUsbMode(0,0)
	#time.sleep(1)
	#vnc.setUsbMode("USB_MODE_KEY|USB_MODE_VNC_HID",0)
	#vnc.setKeyMode("KEY_INTF_HID")
	#testEvent(vnc,10)
	#vnc.setVncMode("MODE_VNC_CAMERA")	#set VNC mode to MSC
	#test_key_ready(vnc)
	#testConnect(3,10)
	print("welcome")
	
def send_test_key(count):
	g=elink.getConnection()
	for i in range(count):
		g[0].sendString("1234567890\n")
		time.sleep(1)
	

def testConnect(count,delay):
	for i in range(count):
		print("connect to eLinkVKM")
		vnc = elink.newConnection("10.42.0.2")
		time.sleep(delay)
		print("close vnc")
		vnc.close()
		time.sleep(1)
		
def test_key_ready(vnc):
	print("waitKeyboard")
	key_state = -1
	
	while True:
		print("first check")
		ev = vnc.getEvent()
		print("check event code ",ev.getIdCode()," ",ev.getData("Test"))
		if ev.getIdCode() == 2:
			print("detect USB ext")
			break

	ev = vnc.getEvent(2000)
	print("event code ",ev.getIdCode()," ",ev.getData("Test"))

	while True:
		print("check event code ",ev.getIdCode()," ",ev.getData("Test"))
		if ev.getIdCode() == 6:
			key_state = ev.getData("Test") & 0x04
			break
		elif ev.getIdCode() == -1:
			break;
		else:
			ev = vnc.getEvent(1000)

	
	if key_state == -1:
		print("send scrolllock")
		key_state = 0
		vnc.sendKey("ScrollLock")
		
	print("Receive set_report ",key_state)
	while True:
		ev = vnc.getEvent(4000)
		if ev.getIdCode() == -1:
			print("send scrolllock")
			vnc.sendKey("ScrollLock")
			continue
		elif ev.getIdCode() == 6:
			print("report ",ev.getIdCode()," ",ev.getData("Test"))
			key_state2 = ev.getData("Test") & 0x04
		else:
			print("event code ",ev.getIdCode()," ",ev.getData("Test"))
			continue
		
		if key_state2 == key_state:
			break
	print("keyboard is ready")
	
def testVncMode(count,delay):
	g=elink.getConnection()
	for i in range(count):
		for j in range(len(g)):
			print("test ",i,"on ",j)
			g[j].setVncMode("MODE_VNC_MSC")
			print("switch to MSC ",j)
			time.sleep(delay)
			g[j].setVncMode("MODE_VNC_DUMMY")
			print("switch to DUMMY ",j)
			time.sleep(delay)
	print("end vnc mode test")

def	testEvent(vnc,count):
	vnc.clrEvent();
	for i in range(count):
		print("getEvent()")
		ev = vnc.getEvent(5000)
		print("event id = ",ev.getIdCode(),"data = ",ev.getData("test"))
	print("end of testEvent")

def example():
	vnc = elink.newConnection("192.168.120.3") #create a connection to Ip address
	vnc.setKeyIdle(10) 		# set keyboard idle time before sending new data
	vnc.setVncIdle(2000) 	#set trigger event if VNC idle for 2 second
	vnc.sendString("Hello World")
	vnc.sendKey("a")			#send acsii
	vnc.sendKey("LeftShift")	#send a special key
	vnc.setKeyMode("KEY_INTF_HID")
	vnc.sendKeyEx(["LeftControl","LeftAlt","DeleteForward"])
	vnc.sendMouse(0,100,100) #move mouse to 100,100
	vnc.sendMouse("RDOWN",100,100)	#right mouse down at 100,100
	vnc.sendMouse("RDOWN|CLICK",100,100) #right mouse click at 100,100	(or 0x40)
	vnc.sendMouse("RDOWN|DCLICK",100,100) #right mouse double click at 100,100 (or 0x80)
	vnc.close()	#close the VNC connection
	vnc.setVncMode("MODE_VNC_MSC")	#set VNC mode to MSC
	vnc.setUsbMode("USB_MODE_KEY|USB_MODE_MOUSE",0,["A:\syslinux.hddx"]) #set USB mode
	vnc.clrEvent()	#clear all events
	ev = vnc.getEvent() #wait for a event
	gvnc = elink.getConnection() #get all active connection
	for i in range(len(gvnc)):
		gvnc[i].sendString("Hello World");
		ev = gnvc.getEvent()

if __name__ == "__main__":
	import time
	main()

	