import unittest
import subprocess
import os
import sys
import argparse
import logging
import datetime

import ELinkLog
from ElinkScript import Utils
from ElinkScript import ScriptRunner

from testsuite.default import *

testlog = logging.getLogger('elinktest')
logger = logging.getLogger('elinkdebug')

# possible values from command line arguments
elinkviewer = ""
testsuite = ""
run_count = 1


# current environment, used for passing scirpt arguments


def generate_hdd_img(isoPath, hddPath):
    # subprocess.call(["ls", "-lha"])
    subprocess.call(["vfimg.exe", "-i", isoPath,
                     "-o", hddPath], env=current_env)


def printTestCases():
    i = 1
    for t in test_cases:
        print(str(i) + ". " + t["key"])
        i = i + 1


class ElinkScriptTest(unittest.TestCase):
    # def test_generate_hdd_img(self):
    #     generate_hdd_img("Win2012.iso", "Win2012.hdd2")
    tc_index = 0
    selected_tc = ""

    @staticmethod
    def getTestCaseNames():
        # print("Test case name")
        return test_cases

    @staticmethod
    def runall():
        print("Test cases: " + str(test_cases))
        for t in test_cases:
            print("t is: " + t)
            print("Get basename")
            print(os.path.splitext(t)[0])
            # os.chdir("testsuite/" + os.path.splitext(t)[0])
            print("current dir: " + os.getcwd())
            ScriptRunner.run(t)

    @staticmethod
    def runtc():
        print("selected tc: " + ElinkScriptTest.selected_tc)
        # os.chdir(os.path.dirname())
        for i in range(run_count):
            print("Run count: " + i)
            subprocess.call(
                [elinkviewer, "-cons=" + ElinkScriptTest.selected_tc], env=current_env)

    @staticmethod
    def run_script():
        t = test_cases[ElinkScriptTest.tc_index - 1]
        for i in range(run_count):
            print("Run count: " + str(i))
            ScriptRunner.run(t)


parser = argparse.ArgumentParser(description='Test driver for eLinkScript')

parser.add_argument('-p', action='store_true', help='Print test cases')
parser.add_argument('-e', help='Select an elinkviewer version')
parser.add_argument('--elinkviewer', help='Select an elinkviewer version')
parser.add_argument('--runtc', help='Run a specific test case by path')
parser.add_argument('--ip', help='Specify an ip address')

# Switch
parser.add_argument('--count', type=int,
                    help='Repeat a test case a number of times')
parser.add_argument('--runall', action='store_true', help='Run a testsuite')
parser.add_argument('--testsuite', help='Run a testsuite',
                    default="testsuite/default.py")
parser.add_argument('--run', type=int, help='Run a specific test case by ID')

args = parser.parse_args()

# print test case list
if args.p:
    printTestCases()
    exit(0)

# select elinkviewer version
ver = (args.e or args.elinkviewer)

if ver:
    elinkviewer = os.path.abspath("elinkviewer_" + ver + ".exe")
else:
    elinkviewer = os.path.abspath("elinkviewer.exe")

ScriptRunner.setElinkViewer(elinkviewer)
print("Select elinkviewer binary: " + elinkviewer)

# select ip
if args.ip:
    print("IP: " + args.ip)
    ScriptRunner.setKvmIp(args.ip)

testsuite = args.testsuite
exec(open(testsuite).read())
# test_cases are defined in the test suite file.
# default testsuite file: testsuite/default.py
print("Loaded test cases: " + str(test_cases))

# check count
if args.count:
    run_count = args.count
print("Run count: " + str(run_count))

exception_str = ""
if args.runall is True:
    suite = unittest.TestSuite()
    suite.addTest(ElinkScriptTest('runall'))
    unittest.TextTestRunner(verbosity=2).run(suite)

elif (args.run is not None) and args.run > 0:
    ElinkScriptTest.tc_index = args.run
    ElinkScriptTest.selected_tc = test_cases[args.run - 1]
    suite = unittest.TestSuite()
    suite.addTest(ElinkScriptTest('run_script'))
    unittest.TextTestRunner(verbosity=2).run(suite)

elif (args.runtc is not None):
    ElinkScriptTest.selected_tc = args.runtc
    suite = unittest.TestSuite()
    suite.addTest(ElinkScriptTest('runtc'))
    unittest.TextTestRunner(verbosity=2).run(suite)
