import sys
import os
import ElinkScript
from ElinkScript import *
from ElinkScript.Debug import *
from ElinkScript.ElinkSystem import *
from time import sleep
sys.path.append(os.getcwd())
print("cwd: " + os.getcwd())

print("IP: " + ElinkSystem.getIP())
args = ScriptRunner.getScriptArg()
print("args is: " + str(args))
vnc = ElinkSystem.connect(elink)
screen = ElinkSystem.getScreen()


def format_disk():
    print(" - Prepare to format disk 0 with diskpart")
    screen.waitImage(winpe_sys32_prompt)
    vnc.setKeyIdle(30)
    sendCommand("diskpart")
    screen.waitImage(diskpart_prompt)
    print(" - Detected diskpart prompt")
    sendCommand("select disk 0")
    sleep(2)

    # for i in range(1,5):
    #     print("Delete partition " + str(i))
    #     erase_partition(vnc, i)
    #     sleep(1)
    sendCommand("clean")
    sleep(1)
    sendCommand("list volume")
    sleep(1)
    sendCommand("exit")
    sleep(1)
    sendCommand("wpeutil reboot")
    sleep(1)


def enter_elinkkvm_usb():
    print(" - Checking for usb enumeration")
    while True:
        e = vnc.getEvent()
        d = e.getData("test")
        dbg("event: " + str(e.getIdCode()))
        dbg("data: " + str(d))
        if e.getIdCode() == 5 and d == 0:
            sleep(0.2)
            dbg("sending F12")
            vnc.sendKey("F12")
            break

    print(" -  Check keyboard ready")
    print(" - Select boot usb for Windows setup")
    dbg("Done sending F12 key...")
    sleep(10)
    for i in range(15):
        dbg("send key down")
        sleep(0.2)
        vnc.sendKey("Down")
    for i in range(4):
        dbg("send key up")
        sleep(0.2)
        vnc.sendKey("Up")
    vnc.sendKey("Enter")


def erase_drive(drive):
    drive.click()
    drive.click()
    delete_btn.click()
    ok_btn.click()


def delete_existing_drives():
    erase_drive(drive4)
    erase_drive(drive3)
    erase_drive(drive2)
    erase_drive(drive1)


def install_on_unallocated_drive():
    ok_btn.click()


def configure_ElinkScript_winpe():
    vnc.setVncMode("MODE_VNC_MSC")
    vnc.clrEvent()
    vnc.setUsbMode("USB_MODE_KEY|USB_MODE_VNC_HID", 0, ["A:\winpe.hdd2"])
    vnc.setKeyMode(1)
    vnc.setMouseMode(2)
    print(" - Viewer configured for WinPe")


def configure_ElinkScript_setup_win2012():
    vnc.setVncMode("MODE_VNC_MSC")
    vnc.clrEvent()
    vnc.setUsbMode("USB_MODE_KEY|USB_MODE_VNC_HID", 0, ["A:\win2012.hdd2"])
    vnc.setKeyMode(1)
    vnc.setMouseMode(2)
    print(" - Viewer configured and Windows 2012 setup image mounted")


def install_windows():
    # vnc.setKeyIdle(30)
    sleep(2)
    print(" - Click Next")
    welcome_next_btn.click()
    print(" - Click Install")
    install_btn.click()
    screen.waitImage(select_os_text)
    print(" - Select edition")
    select_edition_btn.click()
    print(" - Click Next")
    next_btn.click()
    print(" - Tick license box")
    license_check_box.click()
    print(" - Click Next")
    next_btn.click()
    print(" - Select Custom Install")
    custom_install_btn.click()
    print(" - Select drive")
    found = screen.waitImagesOr([drive_msr_reserved, drive0_unallocated])

    if found == drive_msr_reserved:
        delete_existing_drives()
    else:
        install_on_unallocated_drive()

    next_btn.click()
    ok_btn.click()
    ElinkSystem.waitUntilNoChange()


def post_install_windows():
    vnc.setKeyIdle(30)
    print(" - Wait for first boot into Windows")
    found = screen.waitImagesOr([win2012_menu, settings_text])
    if found == win2012_menu:
        vnc.sendKey("Enter")
        print(" - Found Windows Server 2012 menu. Wait until settings")
        screen.waitImage(settings_text)
    print(" - Enter Windows password: Abcdef12345")
    sleep(2)
    vnc.sendString("Abcdef12345")
    sleep(1)
    vnc.sendKey("Tab")
    sleep(1)
    vnc.sendString("Abcdef12345")
    sleep(2)
    print(" - Click Finish")
    finish_btn.clickUntil(ctrl_alt_del_text)
    # screen.waitImage("press_ctrl_alt_del_text.jpg")
    # vnc.sendKeyEx(["LeftControl","LeftAlt","DeleteForward"])
    # sleep(5)
    # vnc.sendString("Abcdef12345")
    # vnc.sendKey("Enter")
    # screen.waitImage("cmd_btn.jpg")
    # sleep(20)
    # clickbutton(vnc,"cmd_btn.jpg")
    # #screen.click(vnc,cmd_btn)
    # print(" - Click and wait for command promopt")
    # waitImage(vnc, "cmd_prompt.png")
    # sleep(20)
    # print(" - Restart Windows")
    # vnc.sendString("shutdown /r")
    # sleep(2)
    # vnc.sendKey("Enter")


def enter_windows_setup():
    print(" - Waiting for Windows Boot Manager or loading files...")
    found = screen.waitImagesOr([setup_menu, loading_files])
    if found == setup_menu:
        print(" - Found Windows Boot Manager menu")
    else:
        print(" - Found loading files")

    sleep(1)
    vnc.sendKey("Enter")
    sleep(1)
    vnc.sendKey("Enter")
    sleep(1)
    vnc.sendKey("Enter")

    ElinkSystem.waitUntilNoChange(2500)
    print(" - Wait for Windows 2012 text")
    found = screen.waitImagesOr([win2012_text, win2012_text2])
    print(" - Entered Windows Setup")


def main():
    pass_count = 0
    print("re-enable keyboard")
    vnc.setUsbMode(0, 0)  # this workaround the bug of KeyOnPower
    time.sleep(2)
    vnc.setUsbMode("USB_MODE_KEY", 0)  # this workaround the bug of KeyOnPower
    vnc.setKeyMode(1)
    # ElinkSystem.waitKeyReady()

    # print("\nSTAGE 1: Configure the viewer for winpe")
    # configure_ElinkScript_winpe()

    # print("\nSTEP 1.1: Enter boot USB from eLinkKVM")
    # enter_elinkkvm_usb()

    # print("\nSTEP 1.2: Format disk")
    # format_disk()

    print("\nSTAGE 2: Configure the viewer for setting up Windows 2012")
    configure_ElinkScript_setup_win2012()

    print("\nSTEP 2.1: Enter boot USB from eLinkKVM")
    enter_elinkkvm_usb()

    print("\nSTEP 2.2: Enter Windows setup")
    enter_windows_setup()

    print("\nSTEP 2.3: Install Windows")
    install_windows()

    print("\nSTEP 2.4: Post-installation setup")
    post_install_windows()


if __name__ == "__main__":
    import time
    main()
