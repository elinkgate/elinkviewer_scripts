from time import sleep
ABS=0
REL=1

def waitUntilNoChange(vnc, delay = None):
	if delay is None:
		delay = 5000
	print("Wait until no change...")
	print("delay = " + str(delay))
	e = vnc.getEvent()

	vnc.setVncIdle(delay)
	while e.getIdCode() != 28:
		e = vnc.getEvent()
		print(" |- event id: " + str(e.getIdCode()))

		if e.getIdCode() == 28:
			print(" Got Idle Event")
			return
			#vnc.sendKey("CapsLock")
		e = vnc.getEvent()

def waitImage(vnc, img):
	loc = None
	while loc == None:
		loc = vnc.matchScreen(img)
		sleep(0.7)

	return loc

def waitImagesOr(vnc, img_list):
	found = None
	while not found:
		for img in img_list:
			found = vnc.matchScreen(img)
			if found:
				return [img, found]
			sleep(0.7)

	return None

def clickPoint(vnc,p,delay = None):
	if delay is None:
		delay = 2

	info = vnc.info()
	print(info)
	w = info[2]
	h = info[3]
	x = int(p[REL][0] * w)
	y = int(p[REL][1] * h)
	x_abs = int(p[ABS][0])
	y_abs = int(p[ABS][1])
	print("x: " + str(x) + " y: " + str(y))
	print("x_abs: " + str(x_abs) + " y_abs: " + str(y_abs))
	vnc.sendMouse("LDOWN|CLICK",x,y) #right mouse click at 100,100	(or 0x40)
	# vnc.setVncIdle(delay)
	sleep(delay)

def boot_elink_mass_storage(vnc):
	print("Checking for usb enumeration...")
	# vnc = elink.newConnection("10.42.0.2")
	# vnc.setVncMode("MODE_VNC_MSC")
	# vnc.setUsbMode("USB_MODE_MOUSE|USB_MODE_KEY|USB_MODE_VNC_HID",0,["A:\win2012.hddx"])
	# vnc.setKeyMode(1)
	# vnc.setMouseMode(2)
	# print("Done configuring viewer...")

	while True:
		e = vnc.getEvent()
		d = e.getData("test")
		print("event: " + str(e.getIdCode()))
		print("data: " + str(d))
		if e.getIdCode() == 5 and d == 0:
			print("sending F12")
			vnc.sendKey("F12")
			break


	vnc.setVncIdle(3000)
	e = vnc.getEvent()
	while e.getIdCode() != 28:
		e = vnc.getEvent()

	print("Done sending key...")
	# sleep(5)
	for i in range(15):
		print("send key down")
		sleep(0.1)
		vnc.sendKey("Down")
	for i in range(10):
		print("send key up")
		sleep(0.1)
		vnc.sendKey("Up")
	vnc.sendKey("Enter")

def erase_drive(vnc, drive):
	clickPoint(vnc, drive)
	clickPoint(vnc, delete_btn)
	clickPoint(vnc, ok_btn)

def erase_partition(vnc, part_id):
	sendCommand(vnc, "select partition " + str(part_id))
	sleep(3)
	sendCommand(vnc, "delete partition override")

def sendCommand(vnc, command):
	vnc.sendString(command)
	vnc.sendKey("Enter")

def main():
	print("main")
	vnc = elink.newConnection("10.42.0.2")
	vnc.setVncMode("MODE_VNC_MSC")
	vnc.setUsbMode("USB_MODE_KEY",0,["A:\winpe.hddx"])
	vnc.setKeyMode(1)
	# vnc.setMouseMode(2)
	vnc.setKeyIdle(50)

	boot_elink_mass_storage(vnc)
	print("Prepare to format disk 0 with diskpart...")
	waitImage(vnc, "winpe_system32_prompt.jpg")
	sendCommand(vnc, "diskpart")
	waitImage(vnc, "diskpart_prompt.jpg")
	print("See diskpart prompt...")
	sendCommand(vnc, "select disk 0")
	sleep(2)

	# for i in range(1,5):
	#     print("Delete partition " + str(i))
	#     erase_partition(vnc, i)
	#     sleep(1)
	sendCommand(vnc, "clean")
	sendCommand(vnc, "list volume")
	sendCommand(vnc, "exit")
	sendCommand(vnc, "wpeutil shutdown")

	# vnc.close()

if __name__ == "__main__":
	import time
	# vnc = elink.newConnection("10.42.0.2")
	# vnc.setVncMode("MODE_VNC_MSC")
	# vnc.setUsbMode("USB_MODE_MOUSE|USB_MODE_KEY|USB_MODE_VNC_HID",0,["A:\win2012.hddx"])
	# vnc.setKeyMode(1)
	# vnc.setMouseMode(2)
	# erase_drive(vnc, drive1)
	# erase_drive(vnc, drive2)
	# erase_drive(vnc, drive3)
	# erase_drive(vnc, drive4)
	main()
