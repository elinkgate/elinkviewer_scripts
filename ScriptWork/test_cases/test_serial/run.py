import random
import string
import elink
from Utility.ELinkSerial import ELinkSerial
from ElinkScript.TestRunner import *
import logging
from ElinkScript import ELinkInfo
from Utility.ELinkUtils import *
from PowerMeter.PowerMeter import PowerMeterController

serial_data = None
testlog = logging.getLogger('elinktest')
logger = logging.getLogger('elinkdebug')
data_length = 1024 * 100

testfile = os.path.join(os.path.curdir, 'testserial')


def generate_serial_content():
    # if os.path.isfile(testfile):
    #     with open(testfile, 'r', encoding='utf-16be') as testfilefd:
    #         content = testfilefd.read()
    #         return content
    # else:
    with open(testfile, 'w') as testfilefd:
        content = ''.join(random.choice(string.ascii_uppercase + string.digits)
                          for _ in range(data_length))
        testfilefd.write(content)
        return content.encode('utf-16be')


def test_serial_read_write(*args):
    dev: ELinkInfo = args[0]
    comport = args[1]
    elinkObj = elink.newConnection(dev.ipAddr)
    elinkserial = ELinkSerial(elinkObj, comport)
    elinkserial.enable_elinkserialmode()
    elinkserial.write(serial_data)
    sleep(1)
    elinkObj.close()
    testlog.info("Test serial write ",
                 extra={'devid': "{}".format(dev.devId),
                        'testid': test_serial_read_write.__name__,
                        'info': 1,
                        }
                 )


if __name__ == '__main__':
    app = TestRunner()
    serial_data = generate_serial_content()
    logger.info("Scanning eLinkKVM device ")
    devs_info = elink.scanDevice()
    logger.info("Found {}".format(len(devs_info)))
    if len(devs_info) == 0:
        logger.info("Not found any dev")
        input("Press any key for exit")
        exit(-1)
    count = 0
    devs = PowerMeterController.get_all_serial_devs()
    comdevs = devs['comport']
    if len(comdevs) == 0:
        logger.info("Not found any dev")
        input("Press any key for exit")
        exit(-1)

    for each in devs_info:
        dev = ELinkInfo(each)
        logger.info("{}".format(each))
        logger.info("run test case for {}".format(dev.devId))
        app.add_test(test_serial_read_write, dev, comdevs[0], testid=dev.devId)
    app.run()
    print(__file__)
