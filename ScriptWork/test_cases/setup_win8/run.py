import sys,os
import ElinkScript
from ElinkScript import *
from ElinkScript.Debug import *
from ElinkScript import Location
from ElinkScript import Pattern
from time import sleep
print("cwd: " + os.getcwd())

attach_drive_btn = Location(346, 502, 0.338, 0.654)
apply_btn = Location(695, 464, 0.679, 0.604)
new_drive_btn = Location(662, 434, 0.646, 0.565)
drive_select_btn = Location(448, 264, 0.438, 0.344)
select_edition_btn = Location(468, 260, 0.457, 0.339)
welcome_next_btn = Location(945, 553, 0.692, 0.405)
next_btn = Location(947, 558, 0.761, 0.727)
drive4 = Location(880, 364, 0.485, 0.471)
drive3 = Location(880, 335, 0.488, 0.436)
drive2 = Location(880, 298, 0.494, 0.388)
drive1 = Location(880, 273, 0.495, 0.339)
finish_btn = Location(859, 697, 0.753, 0.911)
cmd_btn = Location(111, 743, 0.081, 0.967)
express_settings_btn = Location(1055, 676, 0.772, 0.495)
username_loc = Location(274, 322)
enterpass_loc = Location(284, 402)
reenter_pass_loc = Location(284, 446)
account_next_loc = Location(1074, 676)
pass_hint_loc = Location(284, 491)

winpe_sys32_prompt = Pattern("winpe_system32_prompt.jpg")
diskpart_prompt = Pattern("diskpart_prompt.jpg")
select_os_text = Pattern("select_os_text.png")
drive_msr_reserved = Pattern("drive_msr_reserved.png")
drive0_unallocated = Pattern("drive0_unallocated.png")
settings_text = Pattern("settings.png")
setup_menu = Pattern("setup_menu.png")
loading_files = Pattern("loading_files.png")
win10_text = Pattern("win10_text.png")
next_btn = Pattern("next_btn.png")
install_now_btn = Pattern("install_now_btn.png")
skip_key_btn = Pattern("skip_product_key_btn.png")
skip_step_btn = Pattern("skip_step_btn.png")
enter_product_key_text = Pattern("enter_product_key_text.png")
do_later_btn = Pattern("do_this_later_btn.png")
create_an_account_text = Pattern("create_an_account_text.png")
win81_enter_key_textbox = Pattern("win81_enter_key_textbox.png")
custom_install_btn = Pattern("custom_install_text.png")
license_checkbox = Pattern("license_checkbox.png")
drive_icon = Pattern("drive_icon.png")
delete_btn = Pattern("delete_btn.png")
ok_btn = Pattern("ok_btn.png")

win81_key="334NH-RXG76-64THK-C7CKG-D3VPT"

vnc = ElinkSystem.connect(elink)
screen = ElinkSystem.getScreen()

def format_disk():
	print(" - Prepare to format disk 0 with diskpart")
	screen.waitImage(winpe_sys32_prompt)
	vnc.setKeyIdle(30)
	sendCommand("diskpart")
	screen.waitImage(diskpart_prompt)
	print(" - Detected diskpart prompt")
	sendCommand("select disk 0")
	sleep(2)

	# for i in range(1,5):
	#     print("Delete partition " + str(i))
	#     erase_partition(vnc, i)
	#     sleep(1)
	sendCommand("clean")
	sleep(1)
	sendCommand("list volume")
	sleep(1)
	sendCommand("exit")
	sleep(1)
	sendCommand("wpeutil reboot")
	sleep(1)

def enter_elinkkvm_usb():
	print(" - Checking for usb enumeration")
	while True:
		e = vnc.getEvent()
		d = e.getData("test")
		dbg("event: " + str(e.getIdCode()))
		dbg("data: " + str(d))
		if e.getIdCode() == 5 and d == 0:
			sleep(0.2)
			dbg("sending F12")
			vnc.sendKey("F12")
			break

	print(" -  Check keyboard ready")
	print(" - Select boot usb for Windows setup")
	dbg("Done sending F12 key...")
	sleep(10)
	for i in range(15):
		dbg("send key down")
		sleep(0.2)
		vnc.sendKey("Down")
	for i in range(4):
		dbg("send key up")
		sleep(0.2)
		vnc.sendKey("Up")
	vnc.sendKey("Enter")

# def erase_drive(drive):

def delete_existing_drives():
    while not drive0_unallocated.exists(1):
        drive_icon.click()
        delete_btn.click()
        ok_btn.click()

    # erase_drive(drive4)
    # erase_drive(drive3)
    # erase_drive(drive2)
    # erase_drive(drive1)

def install_on_unallocated_drive():
    ok_btn.click()

def configure_ElinkScript_winpe():
    vnc.setVncMode("MODE_VNC_MSC")
    vnc.clrEvent()
    vnc.setUsbMode("USB_MODE_KEY|USB_MODE_VNC_HID",0,["A:\winpe.hdd2"])
    vnc.setKeyMode(1)
    vnc.setMouseMode(2)
    print(" - Viewer configured for WinPe")

def configure_ElinkScript_setup_win2012():
    vnc.setVncMode("MODE_VNC_MSC")
    vnc.clrEvent()
    vnc.setUsbMode("USB_MODE_KEY|USB_MODE_VNC_HID",0,["A:\Win8.hdd2"])
    vnc.setKeyMode(1)
    vnc.setMouseMode(2)
    print(" - Viewer configured and Windows 2012 setup image mounted")

def install_windows():
	# vnc.setKeyIdle(30)
	sleep(2)
	print(" - Click Next")
	next_btn.waitClick()
	# welcome_next_btn.click()
	print(" - Click Install")
	install_now_btn.waitClick()
	print(" - Wait until enter key textbox then click")
	win81_enter_key_textbox.waitClick()
	print(" - Type install key")
	vnc.sendString(win81_key)
	next_btn.waitClick()
	print(" - Tick license box")
	license_checkbox.waitClick()
	print(" - Click Next")
	next_btn.waitClick()
	print(" - Select Custom Install")
	custom_install_btn.waitClick()
	print(" - Select drive")
	found = screen.waitImagesOr([drive_msr_reserved, drive0_unallocated])

	if found == drive_msr_reserved:
		delete_existing_drives()
	else:
		install_on_unallocated_drive()

	next_btn.click()
	ok_btn.click()
	ElinkSystem.waitUntilNoChange()

def post_install_windows():
	vnc.setKeyIdle(30)
	print(" - Wait for first boot into Windows")
	do_later_btn.waitClick()
	skip_step_btn.waitClick()
	express_settings_btn.click()
	screen.waitImage(create_an_account_text)
	username_loc.click()
	vnc.sendString("test")

	print(" - Enter Windows password: Abcdef12345")
	enterpass_loc.click()
	vnc.sendString("Abcdef12345")

	print(" - Re-enter Windows password: Abcdef12345")
	reenter_pass_loc.click()
	vnc.sendString("Abcdef12345")

	print(" - Enter password hint")
	pass_hint_loc.click()

	# password hint must not be equal to password
	# else Windows does not allow it
	vnc.sendString("abcd")

	account_next_loc.click()

def enter_windows_setup():
	print(" - Waiting for Windows Boot Manager or loading files...")
	screen.waitImage(loading_files)
	# found = screen.waitImagesOr([setup_menu, loading_files])
	# if found == setup_menu:
	# 	print(" - Found Windows Boot Manager menu")
	# else:
	# 	print(" - Found loading files")

	sleep(1)
	vnc.sendKey("Enter")
	sleep(1)
	vnc.sendKey("Enter")
	sleep(1)
	vnc.sendKey("Enter")

	ElinkSystem.waitUntilNoChange(2500)
	print(" - Wait for Next button")
	screen.waitImage(next_btn)
	print(" - Entered Windows Setup")

def main():
	pass_count=0
	print("re-enable keyboard")
	vnc.setUsbMode(0,0) #this workaround the bug of KeyOnPower
	time.sleep(2)
	vnc.setUsbMode("USB_MODE_KEY",0) #this workaround the bug of KeyOnPower
	vnc.setKeyMode(1)

	print("\nSTAGE 1: Configure the viewer for setting up Windows 2012")
	configure_ElinkScript_setup_win2012()

	print("\nSTEP 2: Enter boot USB from eLinkKVM")
	enter_elinkkvm_usb()

	print("\nSTEP 3: Enter Windows setup")
	enter_windows_setup()

	print("\nSTEP 4: Install Windows")
	install_windows()

	print("\nSTEP 5: Post-installation setup")
	post_install_windows()

if __name__ == "__main__":
	import time
	main()
