import sys,os
sys.path.append(os.getcwd())
from elinkScriptUtils import *
debug_enable = 0

attach_drive_btn = [[346, 502],[0.338,0.654]]
ok_btn = [ [646,464],[0.631, 0.604] ]
apply_btn = [ [695,464],[0.679, 0.604] ]
new_drive_btn = [ [662,434],[0.646, 0.565] ]
drive_select_btn = [ [448,264],[0.438, 0.344] ]
custom_install_btn = [ [503,352],[0.491, 0.458] ]
license_check_box = [ [247,508],[0.241, 0.661] ]
select_edition_btn = [ [468,260],[0.457, 0.339] ]
next_btn = [ [779,558],[0.761, 0.727] ]
drive4 = [ [497,362],[0.485, 0.471] ]
drive3 = [ [500,335],[0.488, 0.436] ]
drive2 = [ [506,298],[0.494, 0.388] ]
drive1 = [ [507,260],[0.495, 0.339] ]
delete_btn = [ [408,437],[0.398, 0.569] ]
drive_options_btn = [ [707,438],[0.690, 0.570] ]
finish_btn = [ [860,696],[0.840, 0.906] ]
cmd_btn = [ [111,743],[0.081, 0.967] ]

vnc = elink.newConnection("10.42.0.2")
vnc.ipmiConnect("10.42.0.100","ADMIN","ADMIN")

def format_disk():
	print(" - Prepare to format disk 0 with diskpart")
	waitImage(vnc, "winpe_system32_prompt.jpg")
	sendCommand(vnc, "diskpart")
	waitImage(vnc, "diskpart_prompt.jpg")
	print(" - Detected diskpart prompt")
	sendCommand(vnc, "select disk 0")
	sleep(2)

	# for i in range(1,5):
	#     print("Delete partition " + str(i))
	#     erase_partition(vnc, i)
	#     sleep(1)
	sendCommand(vnc, "clean")
	sleep(1)
	sendCommand(vnc, "list volume")
	sleep(1)
	sendCommand(vnc, "exit")
	sleep(1)
	sendCommand(vnc, "wpeutil reboot")
	sleep(1)


def enter_elinkkvm_usb():
	print(" - Waiting for BIOS boot menu")
	loc = [None,None,0]
	while loc[0] == None:
	    loc = vnc.matchScreen("server_bios_menu.png")
	    sleep(0.7)
	    vnc.sendKey("F11")
	print (" - Found boot menu")

	print(" - Select eLinkKVM USB")
	loc = [None,None,0]
	while loc[0] == None:
	    loc = vnc.matchScreen("server_elinkkvm_selected.png", 0.95)
	    if loc[0]:
	        sleep(3)
	        break
	    else:
	        vnc.sendKey("Down")
	    sleep(0.5)
	print (" - eLinkKVM USB selected")

	vnc.sendKey("Enter")
	vnc.setUsbMode(0x41,0,["A:\win2012.hddx"])
	vnc.clrEvent()
	vnc.setKeyMode(1)
	vnc.setMouseMode(3)
	vnc.setKeyIdle(300)

def erase_drive(vnc, drive, drive_img):
	waitImage(vnc, drive_img)
	clickPoint(vnc, drive)
	clickPoint(vnc, delete_btn)
	clickPoint(vnc, ok_btn)

def delete_existing_drives(vnc):
	clickPoint(vnc, drive_options_btn)

	print(" - Detected Primary partition")
	erase_drive(vnc, drive4, "server_partition4.png")
	print(" - Primary partition deleted")

	print(" - Detected MSR (Reserved) partition")
	erase_drive(vnc, drive3, "server_partition3.png")
	print(" - MSR (Reserved) partition deleted")

	print(" - Detected System partition")
	erase_drive(vnc, drive2, "server_partition2.png")
	print(" - System partition deleted")

	erase_drive(vnc, drive1, "server_partition1.png")
	print(" - Detected Recovery partition")
	print(" - Recovery partition deleted")



def install_on_unallocated_drive(vnc):
	clickPoint(vnc, ok_btn)

def configure_elinkviewer_setup_win2012():
	vnc.setVncMode("MODE_VNC_CAMERA")
	vnc.clrEvent()
	vnc.setUsbMode(0x041,0,["A:\win2012.hddx"])
	vnc.setKeyMode(1)
	vnc.setMouseMode(3)
	vnc.setKeyIdle(100)
	print(" - Viewer configured for Windows 2012 setup")

	return vnc

def install_windows():
	sleep(2)
	print(" - Click Next")
	clickPoint(vnc, next_btn)
	print(" - Select edition")
	clickPoint(vnc, select_edition_btn)
	print(" - Click Next")
	clickPoint(vnc, next_btn)
	print(" - Tick license box")
	clickPoint(vnc, license_check_box)
	print(" - Click Next")
	clickPoint(vnc, next_btn)
	print(" - Select Custom Install")
	clickPoint(vnc, custom_install_btn)
	print(" - Select drive")
	delete_existing_drives(vnc)
	# found = waitImagesOr(vnc, ["server_drive_msr_reserved.png", "drive0_unallocated.jpg"])
	# if found[0] == "server_drive_msr_reserved.png":
	# 	print(" - Delete existing drives")
	# else:
	# 	install_on_unallocated_drive(vnc)

	clickPoint(vnc, next_btn)
	clickPoint(vnc, ok_btn)
	# text = " - Waitng for BIOS boot menu... "
	# print(text, end="\r")
	# loc = None
	# while loc == None:
	#     loc = vnc.matchScreen("server_bios_menu.png")
	#     sleep(0.7)
	#     vnc.sendKey("F11")
	# print (text + "Found boot menu.")

	# text = " - Select Windows Boot Manager... "
	# print(text, end="\r")
	# loc = None
	# while loc == None:
	#     loc = vnc.matchScreen("server_windows_bootmgr.png", 0.95)
	#     if loc:
	#         sleep(3)
	#         break
	#     else:
	#         vnc.sendKey("Down")
	#     sleep(0.5)
	# print (text + "Windows Boot Manager selected.")

def post_install_windows():
	print(" - Waiting for first boot into Windows")
	found = waitImagesOr(vnc, ["window_server_2012_menu.jpg", "settings.jpg"],0.8)
	if found[0] == "window_server_2012_menu.jpg":
		vnc.sendKey("Enter")
		print(" - Found Windows Server 2012 menu")

	print(" - Waiting for Settings text")
	waitImage(vnc, "settings.jpg")
	print(" - Found Settings text")
	vnc.sendMouse("LDOWN|CLICK",300,246)
	sleep(2)
	print(" - Enter new password: Abcdef12345")
	sleep(2)
	vnc.sendString("Abcdef12345")
	sleep(2)
	vnc.sendKey("Tab")
	sleep(2)
	print(" - Re-enter password: Abcdef12345")
	vnc.sendString("Abcdef12345")
	sleep(7)
	print(" - Click Finish")
	clickPoint(vnc, finish_btn)
	# waitImage(vnc,"press_ctrl_alt_del_text.jpg")
	# vnc.sendKeyEx(["LeftControl","LeftAlt","DeleteForward"])
	# sleep(1)
	# vnc.sendString("Abcdef12345")
	# vnc.sendKey("Enter")
	# waitImage(vnc,"cmd_btn.jpg")
	# sleep(8)
	# clickPoint(vnc,cmd_btn)
	# print(" - Click and wait for command promopt")
	# waitImage(vnc, "cmd_prompt.png")
	# print(" - Restart Windows")
	# vnc.sendString("shutdown /r")
	# vnc.sendKey("Enter")

def enter_windows_setup():
	print(" - Waiting for Windows Boot Manager or loading files")
	found = waitImagesOr(vnc, ["server_loading_files.png", "setup_menu.jpg","server_win2012_text.png"], 0.8)
	if found[0] == "server_win2012_text.png":
		printf("Found Windows 2012 text")
		return
	elif found[0] == "setup_menu.png":
		print(" - Found Windows Boot Manager menu")
	else:
		print(" - Found \"Loading Files...\"")

	sleep(1)
	vnc.sendKey("Enter")
	sleep(1)
	vnc.sendKey("Enter")
	sleep(1)
	vnc.sendKey("Enter")

	print(" - Wait for Windows 2012 text")
	found = waitImage(vnc, "server_win2012_text.png")
	print(" - Found Windows 2012 text")

def main():
	pass_count=0
	vnc.setUsbMode(0,0)
	sleep(2)
	vnc.setVncMode("MODE_VNC_CAMERA")


if __name__ == "__main__":
	import time
	main()

	
