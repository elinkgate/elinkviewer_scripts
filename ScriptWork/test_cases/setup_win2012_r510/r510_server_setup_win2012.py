import sys,os
sys.path.append(os.getcwd())
from elinkScriptUtils import *
debug_enable = 1

attach_drive_btn = [[346, 502],[0.338,0.654]]
ok_btn = [ [646,464],[0.631, 0.604] ]
apply_btn = [ [695,464],[0.679, 0.604] ]
new_drive_btn = [ [662,434],[0.646, 0.565] ]
drive_select_btn = [ [448,264],[0.438, 0.344] ]
custom_install_btn = [ [503,352],[0.491, 0.458] ]
license_check_box = [ [247,508],[0.241, 0.661] ]
next_btn = [ [579,579],[0.905, 0.905] ]
select_edition_btn = [ [304,304],[0.475, 0.475] ]
drive4 = [ [497,362],[0.485, 0.471] ]
drive3 = [ [500,335],[0.488, 0.436] ]
drive2 = [ [506,298],[0.494, 0.388] ]
drive1 = [ [507,260],[0.495, 0.339] ]
delete_btn = [ [408,437],[0.398, 0.569] ]
drive_options_btn = [ [707,438],[0.690, 0.570] ]
finish_btn = [ [860,696],[0.840, 0.906] ]
cmd_btn = [ [111,743],[0.081, 0.967] ]

vnc = elink.newConnection("10.42.0.2")

def resetServer():
	print("connect to IPMI")
	status = vnc.ipmiConnect("elink-ipmi","root","root")
	if status == 1:
		status = vnc.ipmiStatus()
		if status[0] == 1:
			if status[1] == 1:
				print("Reset server")
				vnc.ipmiReset(0)
			else:
				print("Power On server")
				vnc.ipmiPower(1)
			print("connect to IPMI success")
			return
	raise Exception("Connect to IPMI failed")

	
def setBoosterMode():
	vnc.setVncMode("MODE_VNC_MSC")
	vnc.clrEvent()
	vnc.setKeyMode(2)
	vnc.setMouseMode(2)
	vnc.setKeyIdle(30)



def clickCenter(vnc, obj):
	x = obj[0][0]
	y = obj[0][1]
	w = obj[1][0]
	h = obj[1][1]

	center_x = (x + w)/2
	center_y = (x + h)/2

	vnc.sendMouse("LDOWN|CLICK", center_x, center_y)


def format_disk():
	print(" - Prepare to format disk 0 with diskpart")
	waitImage(vnc, "winpe_system32_prompt.jpg")
	sendCommand(vnc, "diskpart")
	waitImage(vnc, "diskpart_prompt.jpg")
	print(" - Detected diskpart prompt")
	sendCommand(vnc, "select disk 0")
	sleep(2)

	# for i in range(1,5):
	#     print("Delete partition " + str(i))
	#     erase_partition(vnc, i)
	#     sleep(1)
	sendCommand(vnc, "clean")
	sleep(1)
	sendCommand(vnc, "list volume")
	sleep(1)
	sendCommand(vnc, "exit")
	sleep(1)
	sendCommand(vnc, "wpeutil reboot")
	sleep(1)


def enter_elinkkvm_usb():
	print(" - Waiting for BIOS boot menu")	
	while True:
		found = waitImagesOr(vnc,["r510_uefi_menu.png","r510_BIOS_setup_menu.png","r510_server_boot_option.png"],0.8)
		if(found[0] == "r510_server_boot_option.png"):
			vnc.sendKey("F11")
			sleep(1)
		if(found[0] == "r510_uefi_menu.png"):
			vnc.sendKey("F11")
			sleep(1)
			break
		else:
			vnc.sendKey("Escape")

	print (" - Found boot menu")
	
	print(" - Select eLinkKVM USB")
	for n_try in range(10):
		loc = vnc.matchScreen("r510_elinkkvm_selected.png", 0.8)
		dbg("max matching %3.2f" % loc[1])
		if loc[0] != None:
			print("we detect eLinkKVM")
			for i in range(10):
				vnc.sendKey("Down")
				sleep(1)
			vnc.sendKey("Up")
			sleep(1)
			vnc.sendKey("Up")
			sleep(1)
			break
		sleep(2)
	if loc[0] == None:
		raise Exception("No eLinkKVM")
	sleep(0.5)
	print (" - eLinkKVM USB selected")

	vnc.sendKey("Enter")
	vnc.setUsbMode(0x41,0,["A:\win2012.hdd2"])
	vnc.clrEvent()
	vnc.setKeyMode(1)
	vnc.setMouseMode(3)
	vnc.setKeyIdle(30)

def erase_drive(vnc, drive_img):
	clickbutton(vnc, drive_img)
	# clickPoint(vnc, drive)
	clickbutton(vnc, "r510_delete_btn.png")
	clickbutton(vnc, "r510_ok_btn.png")

def delete_existing_drives(vnc):
	# clickPoint(vnc, drive_options_btn)

	print(" - Detect Primary partition")
	erase_drive(vnc, "server_partition4.png")
	print(" - Primary partition deleted")

	print(" - Detect MSR (Reserved) partition")
	erase_drive(vnc, "server_partition3.png")
	print(" - MSR (Reserved) partition deleted")

	print(" - Detect System partition")
	erase_drive(vnc, "server_partition2.png")
	print(" - System partition deleted")

	erase_drive(vnc, "server_partition1.png")
	print(" - Detect Recovery partition")
	print(" - Recovery partition deleted")



def install_on_unallocated_drive(vnc):
	clickPoint(vnc, ok_btn)

def configure_elinkviewer_setup_win2012():
	vnc.setVncMode("MODE_VNC_CAMERA")
	vnc.clrEvent()
	vnc.setUsbMode(0x041,0,["A:\win2012.hdd2"])
	vnc.setKeyMode(1)
	vnc.setMouseMode(3)
	vnc.setKeyIdle(30)
	print(" - Viewer configured for Windows 2012 setup")

	return vnc

def select_window_edition():
	clickbutton(vnc, "r510_win2012_standard.png")
	while True:
		loc=vnc.matchScreen("r510_window_server_edition.png", 0.8, 3000)
		dbg("max matching %3.2f" % loc[1])
		if loc[0] != None:
			print("found eddition")
			break;
		sleep(2)
		dbg("send down")
		vnc.sendKey("Down")
	
def install_windows():
	sleep(2)
	print(" - Click Next")
	clickbutton(vnc, "r510_next_btn.png")
	print(" - Select edition")
	select_window_edition()
	print(" - Click Next")
	clickbutton(vnc, "r510_next_btn.png")
	print(" - Tick license box")
	clickbutton(vnc, "r510_license_box.png")
	print(" - Click Next")
	clickbutton(vnc, "r510_next_btn.png")
	print(" - Select Custom Install")
	clickbutton(vnc, "r510_custom_install.png")
	print(" - Select drive")
	found = waitImagesOr(vnc, ["server_partition4.png", "r510_drive0_unallocated.png"])
	if found[0] == "server_partition4.png":
		print(" - Delete existing drives")
		delete_existing_drives(vnc)
	else:
		install_on_unallocated_drive(vnc)

	clickbutton(vnc, "r510_next_btn.png")
	# vnc.setVncMode("MODE_VNC_CAMERA")
	# vnc.clrEvent()
	# vnc.setKeyMode(1)
	# vnc.setMouseMode(3)
	# vnc.setKeyIdle(100)

def post_install_windows():
	vnc.setKeyIdle(100)
	print(" - Waiting for first boot into Windows")
	while True:
		found = waitImagesOr(vnc, ["r510_window_server_2012_menu.png", "r510_settings.png","r510_BIOS_setup_menu.png"],0.8)
		if found[0] == "r510_window_server_2012_menu.png":
			vnc.sendKey("Enter")
			print(" - Found Windows Server 2012 menu")
			sleep(2)
		elif found[0]=="r510_settings.png":
			print(" - Found Setting menu")
			sleep(2)
			break
		elif found[0]=="r510_BIOS_setup_menu.png":
			print("detect bios menu")
			vnc.sendKey("Escape")
			sleep(2)

	while True:
		sleep(2)
		clickbutton(vnc, "r510_password.png")
		print(" - wait Mouse")
		vnc.waitMouseEmpty(10000)
		sleep(2)
		print(" - Enter new password: Abcdef12345")
		vnc.sendString("Abcdef12345")
		vnc.waitKeyEmpty(10000)
		sleep(2)
		print("send tab")
		vnc.sendKey("Tab")
		vnc.waitKeyEmpty(10000)
		sleep(2)
		print(" - Re-enter password: Abcdef12345")
		vnc.sendString("Abcdef12345")
		vnc.waitKeyEmpty(10000)
		sleep(2)
		print(" - Click Finish")
		clickbutton(vnc, "r510_finish_btn.png")
		found = waitImagesOr(vnc, ["r510_password_not_match.png", "r510_password_complexity_error.png", "press_ctrl_alt_del_text.jpg"],0.8)
		if found[0] == "press_ctrl_alt_del_text.jpg":
			break;
	
#	vnc.setVncMode("MODE_VNC_CAMERA")
#	vnc.clrEvent()
#	vnc.setKeyMode(1)
#	vnc.setMouseMode(3)
#	vnc.setKeyIdle(30)
	# waitImage(vnc,"press_ctrl_alt_del_text.jpg")
	vnc.sendKeyEx(["LeftControl","LeftAlt","DeleteForward"])
	sleep(1)
	vnc.sendString("Abcdef12345")
	vnc.waitKeyEmpty()
	sleep(2)
	vnc.sendKey("Enter")
	# waitImage(vnc,"cmd_btn.jpg")
	# sleep(8)
	# clickPoint(vnc,cmd_btn)
	# print(" - Click and wait for command promopt")
	# waitImage(vnc, "cmd_prompt.png")
	# print(" - Restart Windows")
	# vnc.sendString("shutdown /r")
	# vnc.sendKey("Enter")

def enter_windows_setup():
	# setBoosterMode()
	print(" - Waiting for Windows Boot Manager or loading files")
	found = waitImagesOr(vnc, ["r510_loading_files.png", "r510_setup_menu.png", "server_win2012_text.png"], 0.9)
	if found[0] == "r510_setup_menu.png":
		print(" - Found Windows Boot Manager menu")
		sleep(1)
		vnc.sendKey("Enter")
	else:
		print(" - Found \"Loading Files...\"")

	print(" - Wait for Windows 2012 text")
	found = waitImage(vnc, "server_win2012_text.png")
	print(" - Found Windows 2012 text")

def main():
	pass_count=0
	#vnc.setUsbMode(0,0)
	#sleep(2)
	vnc.setVncMode("MODE_VNC_CAMERA")

	for i in range(1):
		print("Reset Server")
		resetServer()
		sleep(5)

		print("\nSTAGE 1: Configure the viewer")
		configure_elinkviewer_setup_win2012()

		print("\nSTAGE 2: Enter boot USB from eLinkKVM")
		enter_elinkkvm_usb()

		print("\nSTAGE 3: Enter Windows setup")
		enter_windows_setup()

		print("\nSTAGE 4: Install Windows")
		install_windows()

		print("\nSTAGE 5: Post-installation setup")
		post_install_windows()
		# pass_count = pass_count + 1
		sleep(1)
		print("Window Setup Completed")


if __name__ == "__main__":
	import time
	main()

	
