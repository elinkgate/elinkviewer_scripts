import sys
import os
import ElinkScript
import ELinkLog
import logging
import datetime

from pathlib import Path
from ElinkScript import *
from ElinkScript.Debug import *
from ElinkScript.ElinkSystem import *
from ElinkScript.TestRunner import *
from time import sleep
sys.path.append(os.getcwd())
print("cwd: " + os.getcwd())

ecriticallog = logging.getLogger('ecriticallog')
test_file = elink.getLastExec()
testlog = logging.getLogger('elinktest')
logger = logging.getLogger('elinkdebug')
kvm = ElinkSystem(elink)


def configure_elinkkvm(kvm):
    kvm.vnc.setVncMode(0x01)
    kvm.vnc.clrEvent()
    kvm.vnc.setUsbMode("USB_MODE_KEY", 0, ["A:\FlashingColorTest.hdd2"])
    kvm.vnc.setKeyMode(1)
    kvm.vnc.setMouseMode(1)

    print(" - elinkkvm configured  setup image mounted")


def test_flashing_colors(*arg):
    start_time_str = str(datetime.datetime.now())
    print("\nMonitoring ADV output")
    configure_elinkkvm(kvm)

    p = kvm.getScriptPath()
    tc_dir = Path(p).parts[-1]
    print("test file: " + test_file)
    print("test dir: " + tc_dir)

    dev = arg[0]
    while True:
        try:
            sleep(5)
            end_time_str = str(datetime.datetime.now())
            testlog.info("", extra={"devid": dev.get_dev_id(),
                                    "testid": tc_dir,
                                    "script": test_file,
                                    "info_duration": 5,
                                    "start_time": start_time_str,
                                    "end_time": end_time_str})
            sleep(5)
        except Exception as e:
            end_time_str = str(datetime.datetime.now())
            exc_type, exc_obj, exc_tb = sys.exc_info()
            exception_str = "[EXCEPTION]: in " + test_file + \
                ", line " + str(exc_tb.tb_lineno) + ": " + str(e)
            print(exception_str)
            ecriticallog.critical("", extra={"devid": dev.get_dev_id(),
                                             "testid": tc_dir,
                                             "script": test_file,
                                             "ipaddr": ElinkSystem.getIP(),
                                             "start_time": start_time_str,
                                             "end_time": end_time_str,
                                             "exception": exception_str})


if __name__ == '__main__':
    app = TestRunner()
    logger.info("Scanning eLinkKVM device ")
    devs_info = kvm.scanDevices()

    logger.info("Found {}".format(len(devs_info)))
    if len(devs_info) == 0:
        logger.info("Cannot not find any dev")
        exit(0)

    for dev in devs_info:
        vnc = kvm.connect(dev.get_dev_ip())
        logger.info("run test case for {}".format(dev.devId))
        app.add_test(test_flashing_colors, dev)

    app.run()
