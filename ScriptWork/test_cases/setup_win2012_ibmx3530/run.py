from time import sleep
from ElinkScript.ElinkSystem import *
from ElinkScript.Debug import *
from ElinkScript import *
import ElinkScript
import os
import sys

env = os.environ.copy()
cwd = os.getcwd()
sys.path.append(cwd)
print("cwd: " + os.getcwd())
print("Last exec: " + elink.getLastExec())
script_dir = env["ELINKKVM_SCRIPTDIR"]
Pattern.setImgPrefixPath(script_dir)
server_boot_menu = Pattern("server_boot_menu.png")
server_elinkkvm_selected = Pattern("server_elinkkvm_selected.png")
setup_menu = Pattern("setup_menu.png")
loading_files = Pattern("loading_files.png")
win2012_text = Pattern("win2012_text.png")
win2012_text2 = Pattern("win2012_text2.jpg")
settings_text = Pattern("settings.png")
win2012_menu = Pattern("windows_server_2012_menu.png")
select_os_text = Pattern("select_os_text.png")
win2012_standard_btn = Pattern("win2012_standard_edition.png")
# license_check_box = Pattern("license_check_box.png")
license_term_lbl = Pattern("license_term_lbl.png")
license_checked_btn = Pattern("license_checked_btn.png")
install_btn = Pattern("install_btn.png")
drive_msr_reserved = Pattern("drive_msr_reserved.png")
drive0_unallocated = Pattern("drive0_unallocated.png")
ctrl_alt_del_text = Pattern("ctrl_alt_del_text.png")
next_btn = Pattern("next_btn.png")
custom_install_btn = Pattern("custom_install_btn.png")
delete_btn = Pattern("delete_btn.png")
ok_btn = Pattern("ok_btn.png")
new_drive_btn = Pattern("new_drive_btn.png")

# ok_btn = Location(646, 464, 0.631, 0.604)
license_check_box = Location(247, 508, 0.241, 0.661)
# custom_install_btn = Location(503, 352, 0.491, 0.458)
drive4 = Location(497, 362, 0.485, 0.471)
drive3 = Location(500, 335, 0.488, 0.436)
drive2 = Location(506, 298, 0.494, 0.388)
drive1 = Location(507, 260, 0.495, 0.339)
# delete_btn = Location(408, 437, 0.398, 0.569)
finish_btn = Location(860, 696, 0.840, 0.906)
attach_drive_btn = Location(346, 502, 0.338, 0.654)
apply_btn = Location(695, 464, 0.679, 0.604)
drive_select_btn = Location(448, 264, 0.438, 0.344)


# print("IP: " + ElinkSystem.getIP())
DISK_IMAGE = "A:\win2012.iso"
ip = env["ELINKKVM_IP"]
print("ip: " + ip)
# args = ScriptRunner.getScriptArg()
# print("args is: " + str(args))

vnc = ElinkSystem.connect(elink, ip)
screen = ElinkSystem.getScreen()
# vnc.ipmiConnect("192.168.1.5:5920", "USERID", "PASSW0RD")


def format_disk():
    print(" - Prepare to format disk 0 with diskpart")
    screen.waitImage(winpe_sys32_prompt)
    vnc.setKeyIdle(30)
    sendCommand("diskpart")
    screen.waitImage(diskpart_prompt)
    print(" - Detected diskpart prompt")
    sendCommand("select disk 0")
    sleep(2)

    # for i in range(1,5):
    #     print("Delete partition " + str(i))
    #     erase_partition(vnc, i)
    #     sleep(1)
    sendCommand("clean")
    sleep(1)
    sendCommand("list volume")
    sleep(1)
    sendCommand("exit")
    sleep(1)
    sendCommand("wpeutil reboot")
    sleep(1)


def enter_elinkkvm_usb():
    print(" - Waiting for BIOS boot menu")
    loc = [None, None, 0]
    while not server_boot_menu.exists():
        sleep(0.7)
        vnc.sendKey("F12")
    print(" - Found boot menu")

    print(" - Select eLinkKVM USB")
    loc = [None, None, 0]
    while not server_elinkkvm_selected.exists():
        sleep(3)
        vnc.sendKey("Down")
    print(" - eLinkKVM USB selected")
    vnc.sendKey("Enter")


def erase_drive(drive):
    drive.click()
    drive.click()
    delete_btn.clickUntil(ok_btn)
    ok_btn.clickUntil(new_drive_btn)


def delete_existing_drives():
    # delete first drive to make new_drive_btn appears
    # so the remaining drive deletions could be terminated
    # properly
    erase_drive(drive1)
    erase_drive(drive4)
    erase_drive(drive3)
    erase_drive(drive2)

    install_on_unallocated_drive()


def install_on_unallocated_drive():
    unallocated_drive_loc = Location(353, 262, 0.0, 0.0)
    while next_btn.exists():
        unallocated_drive_loc.click()
        next_btn.click()
        ok_btn.click()


def configure_ElinkScript_winpe():
    vnc.setVncMode("MODE_VNC_MSC")
    vnc.clrEvent()
    vnc.setUsbMode("USB_MODE_KEY|USB_MODE_VNC_HID", 0, ["A:\winpe.hdd2"])
    vnc.setKeyMode(1)
    vnc.setMouseMode(2)
    print(" - Viewer configured for WinPe")


def configure_ElinkScript_setup_win2012():
    vnc.setVncMode("MODE_VNC_CAMERA")
    # vnc.setVncMode("MODE_VNC_MSC")
    vnc.clrEvent()
    # vnc.setUsbMode("USB_MODE_KEY|USB_MODE_VNC_HID", 0, [DISK_IMAGE])
    vnc.setUsbMode(0x041, 0, [DISK_IMAGE])
    vnc.setKeyMode(1)
    vnc.setMouseMode(3)
    print(" - Viewer configured and Windows 2012 setup image mounted")


def install_windows():
    # vnc.setKeyIdle(30)
    # sleep(2)
    print(" - Click Next")
    next_btn.clickUntil(install_btn)
    print(" - Click Install")
    install_btn.clickUntil(select_os_text)
    # screen.waitImage(select_os_text)
    print(" - Select edition")
    win2012_standard_btn.click()
    print(" - Click Next")
    next_btn.clickUntil(license_term_lbl)
    print(" - Tick license box")
    time.sleep(3)
    license_check_box.click()
    print(" - Click Next")
    next_btn.clickUntil(custom_install_btn)
    print(" - Select Custom Install")
    while custom_install_btn.exists():
        custom_install_btn.click()
    print(" - Select drive")
    found = screen.waitImagesOr([drive_msr_reserved, drive0_unallocated])

    if found.getFileName() == drive_msr_reserved.getFileName():
        print("Found MSR")
        delete_existing_drives()
    else:
        print("Found unallocated drive")
        install_on_unallocated_drive()

    ElinkSystem.waitUntilNoChange()


def post_install_windows():
    vnc.setKeyIdle(30)
    print(" - Wait for first boot into Windows")
    found = screen.waitImagesOr([win2012_menu, settings_text])
    if found == win2012_menu:
        vnc.sendKey("Enter")
        print(" - Found Windows Server 2012 menu. Wait until settings")
        screen.waitImage(settings_text)
    print(" - Enter Windows password: Abcdef12345")
    sleep(2)
    vnc.sendString("Abcdef12345")
    sleep(1)
    vnc.sendKey("Tab")
    sleep(1)
    vnc.sendString("Abcdef12345")
    sleep(2)
    print(" - Click Finish")
    finish_btn.clickUntil(ctrl_alt_del_text)
    # screen.waitImage("press_ctrl_alt_del_text.jpg")
    # vnc.sendKeyEx(["LeftControl","LeftAlt","DeleteForward"])
    # sleep(5)
    # vnc.sendString("Abcdef12345")
    # vnc.sendKey("Enter")
    # screen.waitImage("cmd_btn.jpg")
    # sleep(20)
    # clickbutton(vnc,"cmd_btn.jpg")
    # #screen.click(vnc,cmd_btn)
    # print(" - Click and wait for command promopt")
    # waitImage(vnc, "cmd_prompt.png")
    # sleep(20)
    # print(" - Restart Windows")
    # vnc.sendString("shutdown /r")
    # sleep(2)
    # vnc.sendKey("Enter")


def enter_windows_setup():
    print(" - Waiting for Windows Boot Manager or loading files...")
    while not (setup_menu.exists() or loading_files.exists()):
        vnc.sendKey("Enter")
    # found = screen.waitImagesOr([setup_menu, loading_files])
    # if found == setup_menu:
    #     print(" - Found Windows Boot Manager menu")
    # else:
    #     print(" - Found loading files")

    # sleep(1)
    # vnc.sendKey("Enter")
    # sleep(1)
    # vnc.sendKey("Enter")
    # sleep(1)
    # vnc.sendKey("Enter")

    ElinkSystem.waitUntilNoChange(2500)
    print(" - Wait for Windows 2012 text")
    found = screen.waitImagesOr([win2012_text, win2012_text2])
    print(" - Entered Windows Setup")


def main():
    pass_count = 0
    print("re-enable keyboard")
    vnc.setUsbMode(0, 0)  # this workaround the bug of KeyOnPower
    time.sleep(2)
    vnc.setUsbMode("USB_MODE_KEY", 0)  # this workaround the bug of KeyOnPower
    vnc.setKeyMode(1)
    # ElinkSystem.waitKeyReady()

    # print("\nSTAGE 1: Configure the viewer for winpe")
    # configure_ElinkScript_winpe()

    # print("\nSTEP 1.1: Enter boot USB from eLinkKVM")
    # enter_elinkkvm_usb()

    # print("\nSTEP 1.2: Format disk")
    # format_disk()
    # vnc.ipmiReset(0)

    print("\nSTAGE 2: Configure the viewer for setting up Windows 2012")
    configure_ElinkScript_setup_win2012()

    print("\nSTEP 2.1: Enter boot USB from eLinkKVM")
    enter_elinkkvm_usb()

    print("\nSTEP 2.2: Enter Windows setup")
    enter_windows_setup()

    print("\nSTEP 2.3: Install Windows")
    install_windows()

    print("\nSTEP 2.4: Post-installation setup")
    post_install_windows()


if __name__ == "__main__":
    import time
    main()
