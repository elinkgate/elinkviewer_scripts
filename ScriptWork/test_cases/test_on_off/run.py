import concurrent

from apscheduler.schedulers.asyncio import AsyncIOScheduler

from ElinkScript import ELinkInfo
from ElinkScript.TestRunner import TestRunner
from Utility.ELinkTemp import *
from Utility.ELinkUtils import *
from PowerMeter.PowerMeter import *
import ELinkLog
from Utility.ELinkUtils import *
import elink

try:
    import asyncio
except ImportError:
    import trollius as asyncio
elinkkvm_toshiba_emmc = "Elink-KVM-44:a3:bd"
elinkkvm_sandisk_emmc = "Elink-KVM-44:a3:b1"  # TODO add sandisk Elinkkvm Id here

testScheduler = AsyncIOScheduler()
testScheduler.start()
count = 0
logger = logging.getLogger('elinkdebug')
testlog = logging.getLogger('elinktest')
ecriticallog = logging.getLogger('ecriticallog')

executor = concurrent.futures.ThreadPoolExecutor(max_workers=3)


def on_off_connect_disconnect(*args):
    # logger.info("power off")
    dev: ELinkInfo = args[0]
    powermeter: PowerMeterController = args[1]

    power_on(powermeter)
    try:
        wait_elinkkvm_ready(dev.ipAddr)
        elinkobj = elink_newconnectionnonblocking(elink,
                                                  dev)
        tempmonitor = ELinkTemp(elinkobj)
        tempmonitor.start()
        elink_setVncModeNonBlocking(elinkobj, VncMode.VNC_MODE_RGB)
        sleep(60 * 60)  # TODO fixed me why need timeout here.
        tempmonitor.join()
    except Exception as e:
        ecriticallog.critical("Exception => restart dev".format(dev.get_dev_id()), extra={"devid": dev.get_dev_id(),
                                                                                          "testid": "test_on_off_vga_dev",
                                                                                          "ipaddr": dev.get_dev_ip(),
                                                                                          "exception": 'timeout error'})

        power_off(powermeter)
    testlog.info("test on/off device",
                 extra={'devid': "{}".format(dev.devId),
                        'testid': "test_on_off_vga_dev",
                        'info': 1}
                 )
    power_off(powermeter)


def power_on(powermeter: PowerMeterController):
    powermeter.sendSetSwChannel(InaChannel.PRIMARY_POWER_BUS, 0x01)
    powermeter.sendSetSwChannel(InaChannel.SECONDARY_POWER_BUS, 0x01)


def power_off(powermeter: PowerMeterController):
    powermeter.sendSetSwChannel(InaChannel.PRIMARY_POWER_BUS, 0x00)
    powermeter.sendSetSwChannel(InaChannel.SECONDARY_POWER_BUS, 0x00)


if __name__ == '__main__':
    logger.info("Scanning power meter device")
    serialports = PowerMeterController.scan_com_port()
    if len(serialports) == 0:
        logger.critical("Not found Serial port")
        input('Press any key for exit')
        exit(-1)
    else:
        logger.info("found {} serial port".format(serialports))
    logger.info("start scan device")
    devs_info = elink.scanDevice()
    logger.info("Found {}".format(len(devs_info)))
    if len(devs_info) == 0:
        logger.critical("Not found any dev")
        input('Press any key for exit')
        exit(0)
    powercontroller = PowerMeterController('Comport', serialports[0])
    #  Create test runner and add test function to test runner
    app = TestRunner()
    for each in devs_info:
        dev = ELinkInfo(each)
        logger.info("Setup test On_Off for dev {} {}".format(dev.ipAddr, dev.devId))
        app.add_test(on_off_connect_disconnect, dev, powercontroller)
    try:
        app.run()
    except Exception as e:
        logger.critical("exception : {} ".format(e))
