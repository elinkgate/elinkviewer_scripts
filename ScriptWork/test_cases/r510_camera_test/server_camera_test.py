from time import sleep

ABS=0
REL=1

vnc = elink.newConnection("10.42.0.2")

def sendCommand(vnc, command):
	vnc.sendString(command)
	sleep(0.5)
	vnc.sendKey("Enter")
	sleep(0.5)

def waitUntilNoChange(vnc, delay = None):
	if delay is None:
		delay = 5000
	# print("Wait until no change...")
	# print("delay = " + str(delay))
	e = vnc.getEvent()

	vnc.setVncIdle(delay)
	while e.getIdCode() != 28:
		e = vnc.getEvent()
		# print(" |- event id: " + str(e.getIdCode()))

		if e.getIdCode() == 28:
			return
		e = vnc.getEvent()

def waitImage(vnc, img):
	loc = None
	while loc == None:
		loc = vnc.matchScreen(img)
		sleep(0.7)

	return loc

def waitImagesOr(vnc, img_list):
	found = []
	while not found:
		for img in img_list:
			found = vnc.matchScreen(img)
			if found:
				return [img, found]
			sleep(0.7)

	return found

def clickPoint(vnc,p,delay = None):
	if delay is None:
		delay = 2

	info = vnc.info()
	print(info)
	w = info[2]
	h = info[3]
	x = int(p[REL][0] * w)
	y = int(p[REL][1] * h)
	x_abs = int(p[ABS][0])
	y_abs = int(p[ABS][1])
	print("x: " + str(x) + " y: " + str(y))
	print("x_abs: " + str(x_abs) + " y_abs: " + str(y_abs))
	vnc.sendMouse("LDOWN|CLICK",x,y) #right mouse click at 100,100	(or 0x40)
	# vnc.setVncIdle(delay)
	sleep(delay)


def enter_elinkkvm_usb():
    print("Waitng for BIOS menu...")
    loc = None
    while loc == None:
        loc = vnc.matchScreen("server_bios_menu.png", 0.5)
        sleep(0.7)
        print("not found")
        # vnc.sendKey("DeleteForward")

    print("Found image at " + str(loc))

    print("Going to enter save and exit menu...")
    loc = None
    count = 0
    while loc == None:
        loc = vnc.matchScreen("save_and_exit.png", 0.70)
        print("\nloc is: " + str(loc))
        if loc:
            print("Found image at " + str(loc))
            print("Going to break")
            break
        else:
            print("send key right")
            vnc.sendKey("Right")
            count += 1
            print("Count: " + str(count))

        sleep(0.4)

    loc = None
    while loc == None:
        loc = vnc.matchScreen("server_nxp_selected.png", 0.85)
        if loc:
            break
        else:
            vnc.sendKey("Down")
        sleep(0.5)

    print("Found image at " + str(loc))

def configure_elinkviewer_setup_win2012():
	vnc.setVncMode("MODE_VNC_CAMERA")
	vnc.setUsbMode("USB_MODE_KEY",0,["A:\win2012.hddx"])
	vnc.clrEvent()
	vnc.setKeyMode(1)
	# vnc.setMouseMode(2)
	vnc.setKeyIdle(20)
	print(" - Viewer configured and Windows 2012 setup image mounted")

	return vnc

def main():
	print("\nSTEP 0: Configure the viewer for winpe")
	configure_elinkviewer_setup_win2012()

	print("\nSTEP 0.1: Enter boot USB from eLinkKVM")
	enter_elinkkvm_usb()

if __name__ == "__main__":
	import time
	main()
