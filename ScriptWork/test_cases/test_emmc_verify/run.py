import elink
from Utility.FileManager import FileManager
from Utility.ELinkUtils import *
from ElinkScript import ELinkInfo
from ElinkScript.TestRunner import *
from PowerMeter.PowerMeter import *
import ELinkLog

file_size = (1024 * 1024 * 1024 * 1024)  # 1MB
test_file = "test.bin"

testlog = logging.getLogger('elinktest')
logger = logging.getLogger('elinkdebug')
ecriticallog = logging.getLogger('ecriticallog')


def check_test_file_exist():
    if os.path.isfile(test_file):
        return True
    else:
        create_upload_file()
        if os.path.isfile(test_file):
            return True
        else:
            Exception("not foundction ReformatCode test file")


def create_upload_file():
    f = open(test_file, 'wb')
    f.seek(1073741824 - 1)  # 1073741824 bytes in 1 GiB
    f.write(bytes('abc\0'.encode(encoding='UTF-8')))
    f.close()


def remove_and_upload_file(*args):
    dev: ELinkInfo = args[0]
    powermeter: PowerMeterController = args[1]
    try:
        powermeter.power_on()
        wait_elinkkvm_ready(dev.ipAddr)
        elinkObj = elink.newConnection(dev.ipAddr)
        logger.info("{} typ of elink Obj".format(type(elinkObj)))
        sleep(1)  # TODO fixed me why need timeout here.
        filemnger = FileManager(elinkObj, dev)
        if not check_test_file_exist():
            Exception("Test Error")
        viewerpath = filemnger.upload_file(test_file,
                                           "/A:/")
        elinkObj.close()
        testlog.info("write erase emmc",
                     extra={'devid': "{}".format(dev.devId),
                            'testid': "test_write_erase_emmc",
                            'info': 1,
                            }
                     )
        powermeter.power_off()
        powermeter.power_on()
        wait_elinkkvm_ready(dev.ipAddr)
        elinkObj = elink.newConnection(dev.ipAddr)
        filemnger = FileManager(elinkObj, dev)
        ret = filemnger.ssh_verify_md5(test_file, viewerpath)
        if not ret:
            ecriticallog.critical("Exception => not match md5".format(dev.get_dev_id()),
                                  extra={"devid": dev.get_dev_id(),
                                         "testid": "test_on_off_vga_dev",
                                         "ipaddr": dev.get_dev_ip(),
                                         "exception": "Not matching md5"})
        elinkObj.close()
        powermeter.power_off()
    except Exception as e:
        ecriticallog.critical("Exception => restart dev".format(dev.get_dev_id()), extra={"devid": dev.get_dev_id(),
                                                                                          "testid": "test_on_off_vga_dev",
                                                                                          "ipaddr": dev.get_dev_ip(),
                                                                                          "exception": e})


if __name__ == '__main__':
    app = TestRunner()
    logger.info("Scanning power meter")
    serial_devs = PowerMeterController.scan_com_port()
    if not len(serial_devs):
        input('press any key to exit')
        exit(0)
    powermeter = PowerMeterController(port=serial_devs[0])
    logger.info("Scanning eLinkKVM device ")
    devs_info = elink.scanDevice()
    logger.info("Found {}".format(len(devs_info)))
    if len(devs_info) == 0:
        logger.info("Not found any elinkkvm dev")
        input('press any key to exit')
        exit(0)
    count = 0
    for each in devs_info:
        dev = ELinkInfo(each)
        logger.info("{}".format(each))
        logger.info("run test case for {}".format(dev.devId))
        app.add_test(remove_and_upload_file, dev, powermeter)
    app.run()
