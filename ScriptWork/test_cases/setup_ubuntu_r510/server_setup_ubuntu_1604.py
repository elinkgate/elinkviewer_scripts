import sys
import os

sys.path.append(os.getcwd())
from elinkScriptUtils import *

attach_drive_btn = [[346, 502], [0.338, 0.654]]
ok_btn = [[646, 464], [0.631, 0.604]]
apply_btn = [[695, 464], [0.679, 0.604]]
new_drive_btn = [[662, 434], [0.646, 0.565]]
drive_select_btn = [[448, 264], [0.438, 0.344]]
custom_install_btn = [[503, 352], [0.491, 0.458]]
license_check_box = [[247, 508], [0.241, 0.661]]
select_edition_btn = [[468, 260], [0.457, 0.339]]
next_btn = [[779, 558], [0.761, 0.727]]
drive4 = [[497, 362], [0.485, 0.471]]
drive3 = [[500, 335], [0.488, 0.436]]
drive2 = [[506, 298], [0.494, 0.388]]
drive1 = [[507, 260], [0.495, 0.339]]
delete_btn = [[408, 437], [0.398, 0.569]]
drive_options_btn = [[707, 438], [0.690, 0.570]]
finish_btn = [[1028, 700], [0.753, 0.911]]
cmd_btn = [[111, 743], [0.081, 0.967]]

vnc = elink.newConnection("10.42.0.2")

vnc.ipmiConnect("10.42.0.102", "ADMIN", "ADMIN")


def waitImage3s(vnc, img, score=0.9):
    return waitImage(vnc, img)


def waiting_for_usb_enumeration():
    print(" - Checking for usb enumeration")

    while True:
        e = vnc.getEvent()
        d = e.getData("test")
        dbg("event: " + str(e.getIdCode()))
        dbg("data: " + str(d))
        if e.getIdCode() == 5 and d == 0:
            break


def enter_elinkkvm_usb():
    print(" - Checking for usb enumeration")

    while True:
        e = vnc.getEvent()
        d = e.getData("test")
        dbg("event: " + str(e.getIdCode()))
        dbg("data: " + str(d))
        if e.getIdCode() == 5 and d == 0:
            dbg("sending F12")
            vnc.sendKey("F12")
            break

    print(" -  Check keyboard ready")
    print(" - Select boot usb for Windows setup")
    dbg("Done sending F12 key...")
    sleep(5)
    for i in range(15):
        dbg("send key down")
        sleep(0.2)
        vnc.sendKey("Down")
    for i in range(4):
        dbg("send key up")
        sleep(0.2)
        vnc.sendKey("Up")
    vnc.sendKey("Enter")


def configure_elinkviewer_setup_ubuntuserver1604():
    vnc.setVncMode("MODE_VNC_MSC")
    vnc.clrEvent()
    vnc.setUsbMode("USB_MODE_KEY|USB_MODE_VNC_HID", 0, ["A:\\ubuntu.hdd2"])
    vnc.setKeyMode(1)
    vnc.setMouseMode(2)
    vnc.setKeyIdle(300)
    print(" - Viewer configured and ubuntu server 16.04 setup image mounted")
    return vnc


def linux_setup_set_password():
    waitImage3s(vnc, "13_linux_set_passwd11.png")
    vnc.sendString(" ")
    sleep(1)
    vnc.sendKey("Enter")
    waitImage3s(vnc, "14_linux_set_passwd_cfm.png")
    vnc.sendString(" ")
    vnc.sendKey("Enter")
    waitImage3s(vnc, "15_linux_set_passwd_weak.png")
    vnc.sendKey("Left")
    sleep(1)
    vnc.sendKey("Enter")
    waitImage3s(vnc, "16_linux_set_encrypt_home.png")
    vnc.sendKey("Enter")


def linux_setup_set_clock():
    waitImage3s(vnc, "17_linux_config_clock.png", 0.85)
    vnc.sendKey("Enter")


def linux_setup_disk_partion():
    waitImage3s(vnc, "18_parition_disk1.png")
    waitImage3s(vnc, "18_partition_disk11.png")
    vnc.sendKey("Left")
    vnc.sendKey("Enter")
    found = waitImagesOr(vnc, ["19_paritition_disk_method.png", "25_partition_disk_force_uefi.png"])
    if found[0] == "19_paritition_disk_method.png":
        vnc.sendKey("Enter")
    elif found[0] == "25_partition_disk_force_uefi.png":
        vnc.sendKey("Left")
        sleep(1)
        vnc.sendKey("Enter")
    else:
        print(" not found anything")
    waitImage3s(vnc, "20_partition_disk_select_disk.png")
    vnc.sendKey("Enter")

    found = waitImagesOr(vnc, ["21_partition_disk_LVM_existed.png", "21_partition_disk_wr_config.png"])
    sleep(3)
    if found[0] == "21_partition_disk_LVM_existed.png":
        print(" - Found Windows Boot Manager menu")
        print(" - Already existed LVM logical volumes")
        vnc.sendKey("Left")
        vnc.sendKey("Enter")
    elif found[0] == "21_partition_disk_wr_config.png":
        print(" - Found \"Loading Files...\"")
    else:
        print(" not found anything exit")
    waitImage3s(vnc, "21_partition_disk_wr_config.png")
    vnc.sendKey("Left")
    sleep(1)
    vnc.sendKey("Enter")
    waitImage3s(vnc, "22_partition_disk_group_parititon.png")
    vnc.sendKey("Enter")
    waitImage3s(vnc, "25_partition_disk_force_uefi.png")
    vnc.sendKey("Left")
    sleep(1)
    vnc.sendKey("Enter")
    waitImage3s(vnc, "25_partition_disk_write_change.png")
    vnc.sendKey("Left")
    sleep(3)
    vnc.sendKey("Enter")
    waitImage3s(vnc, "26_config_task.png")
    vnc.sendKey("Enter")
    waitImage3s(vnc, "27_software_select.png")
    vnc.sendKey("Enter")
    waitImage3s(vnc, "28_installation_complete.png")
    vnc.sendKey("Enter")


def ubuntu_server_setup():
    waitImage3s(vnc, "0_linux_grub_menu.png")
    vnc.sendKey("Enter")
    waitImage3s(vnc, "1_linux_select_lang.png")
    vnc.sendKey("Enter")
    waitImage3s(vnc, "2_linux_select_loc.png")
    vnc.sendKey("Enter")
    waitImage3s(vnc, "3_linux_config_kb_dialog1.png")
    waitImage3s(vnc, "4_linux_config_kb_msg1_1.png")
    vnc.sendKey("Enter")
    waitImage3s(vnc, "5_linux_config_kb_country3.png")
    vnc.sendKey("Enter")
    waitImage3s(vnc, "6_linux_config_kb_layout4.png")
    vnc.sendKey("Enter")
    found = waitImagesOr(vnc, ["8_linux_config_primary_intf.png", "7_linux_config_network.png"])
    sleep(3)
    if found[0] == "8_linux_config_primary_intf.png":
        print(" - Found primary network interface: Enter to continue")
        vnc.sendKey("Enter")
    elif found[0] == "7_linux_config_network.png":
        print(" - Found Fail network configure")
    else:
        print(" not found anything exit")
    waitImage3s(vnc, "8_linux_config_network6.png")
    waitImage3s(vnc, "7_linux_config_network.png")
    vnc.sendKey("Enter")
    sleep(1)
    vnc.sendKey("Enter")
    waitImage3s(vnc, "7_linux_config_network.png")
    vnc.sendKey("Enter")
    waitImage3s(vnc, "9_linux_configure_network_method7.png")
    vnc.sendKey("Down")
    sleep(1)
    vnc.sendKey("Enter")
    waitImage3s(vnc, "10_linux_config_network_hostname8.png")
    vnc.sendKey("Enter")
    waitImage3s(vnc, "11_linux_set_user9.png")
    vnc.sendString("kevinelg")
    vnc.sendKey("Enter")
    waitImage3s(vnc, "12_linux_set_user10.png")
    vnc.sendKey("Enter")
    linux_setup_set_password()
    linux_setup_set_clock()
    linux_setup_disk_partion()
    pass


def remount_ubuntu_setup_after_reject():
    waiting_for_usb_enumeration()
    vnc.setUsbMode(0, 0)  # this workaround the bug of KeyOnPower
    sleep(2)
    vnc.setUsbMode("USB_MODE_KEY|USB_MODE_VNC_HID", 0, ["A:\\ubuntu.hddx"])
    vnc.sendKeyEx(["LeftControl", "LeftAlt", "DeleteForward"])
    waiting_for_usb_enumeration()


def server_ubuntu_server_config():
    vnc.setVncMode("MODE_VNC_CAMERA")
    vnc.clrEvent()
    vnc.setUsbMode(0x041, 0, ["A:\\ubuntu.hdd2"])
    vnc.setKeyMode(1)
    vnc.setMouseMode(3)
    vnc.setKeyIdle(100)
    print(" - Viewer configured for ubuntu 16. setup")


def main():
    print("re-enable keyboard")
    time.sleep(2)
    vnc.setUsbMode("USB_MODE_KEY", 0)  # this workaround the bug of KeyOnPower
    time.sleep(2)
    vnc.setKeyMode(1)
    waitKeyReady(vnc)
    for i in range(1):
        vnc.sendKeyEx(["LeftControl", "LeftAlt", "DeleteForward"])
        configure_elinkviewer_setup_ubuntuserver1604()
        enter_elinkkvm_usb()
        vnc.clrEvent()
        ubuntu_server_setup()
        sleep(2)
        waiting_for_usb_enumeration()
        vnc.setUsbMode("USB_MODE_KEY", 0)  # this workaround the bug of KeyOnPower
        time.sleep(2)
        vnc.setKeyMode(1)
        vnc.setUsbMode("USB_MODE_KEY|USB_MODE_VNC_HID", 0, ["A:\\ubuntu.hddx"])
        vnc.sendKeyEx(["LeftControl", "LeftAlt", "DeleteForward"])


def enter_elinkkvm_usb_server():
    print(" - Waiting for BIOS boot menu")
    loc = [None, None, 0]
    while loc[0] == None:
        loc = vnc.matchScreen("server_bios_menu.png")
        sleep(0.7)
        vnc.sendKey("F11")
    print(" - Found boot menu")

    print(" - Select eLinkKVM USB")
    loc = [None, None, 0]
    while loc[0] == None:
        loc = vnc.matchScreen("server_elinkkvm_selected.png", 0.95)
        if loc[0]:
            sleep(3)
            break
        else:
            vnc.sendKey("Down")
        sleep(0.5)
    print(" - eLinkKVM USB selected")
    #
    vnc.sendKey("Enter")
    vnc.setUsbMode(0x41, 0, ["A:\\ubuntu.hddx"])
    vnc.clrEvent()
    vnc.setKeyMode(1)
    vnc.setMouseMode(3)
    vnc.setKeyIdle(300)


def linux_setup_run():
    ubuntu_server_setup()
    sleep(2)
    waiting_for_usb_enumeration()
    vnc.setUsbMode("USB_MODE_KEY", 0)  # this workaround the bug of KeyOnPower
    time.sleep(2)
    vnc.setKeyMode(1)
    vnc.setUsbMode("USB_MODE_KEY|USB_MODE_VNC_HID", 0, ["A:\\ubuntu.hddx"])
    vnc.sendKeyEx(["LeftControl", "LeftAlt", "DeleteForward"])


def server_setup():
    print("Reset Server")
    vnc.ipmiReset(0)
    sleep(5)
    print("\nSTAGE 1: Configure the viewer")
    server_ubuntu_server_config()
    print("\nSTAGE 2: Enter boot USB from eLinkKVM")
    enter_elinkkvm_usb_server()
    vnc.setVncMode("MODE_VNC_MSC")


def run_setup_ubuntu_for_server():
    server_setup()
    linux_setup_run()
    pass


if __name__ == "__main__":
    import time

    run_setup_ubuntu_for_server()
    # main()
