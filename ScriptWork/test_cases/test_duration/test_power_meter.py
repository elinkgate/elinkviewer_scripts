import unittest
from PowerMeter.PowerMeter import *


class TestPowerMeter(unittest.TestCase):
    def setUp(self):
        result = PowerMeterController.scan_com_port()
        if len(result) == 0:
            raise Exception("not found any Serial port")
        self.serialport = PowerMeterController(__file__, port=result[0])
        pass

    def tearDown(self):
        self.serialport.comport.close()

    # def test_scan_serial_port(self):
    #     result = PowerMeterController.scan_com_port()
    #     print(result)
    #     self.assertEqual(2, len(result))
    def test_power_on_off_channel1(self):
        self.assertTrue(True)
        return
        self.serialport.sendSetSwChannel(InaChannel.PRIMARY_POWER_BUS, PowerState.STATE_ON)
        result = self.serialport.sendGetChannelStatus(InaChannel.PRIMARY_POWER_BUS)
        self.assertEqual(result[0], 1)
        self.serialport.sendSetSwChannel(InaChannel.PRIMARY_POWER_BUS, PowerState.STATE_OFF)
        result = self.serialport.sendGetChannelStatus(InaChannel.PRIMARY_POWER_BUS)
        self.assertEqual(result[0], 0)

    def test_power_off(self):
        self.assertTrue(True)
        return
        self.serialport.sendSetSwChannel(InaChannel.SECONDARY_POWER_BUS, PowerState.STATE_ON)
        result = self.serialport.sendGetChannelStatus(InaChannel.SECONDARY_POWER_BUS)
        self.assertEqual(result[0], 1)
        self.serialport.sendSetSwChannel(InaChannel.SECONDARY_POWER_BUS, PowerState.STATE_OFF)
        result = self.serialport.sendGetChannelStatus(InaChannel.SECONDARY_POWER_BUS)
        self.assertEqual(result[0], 0)

    def test_get_data_channel(self):
        result = self.serialport.sendGetDataChannel(InaChannel.PRIMARY_POWER_BUS)
        self.assertEqual(result.channelId, InaChannel.PRIMARY_POWER_BUS)
        result = self.serialport.sendGetDataChannel(InaChannel.SECONDARY_POWER_BUS)
        self.assertEqual(result.channelId, InaChannel.SECONDARY_POWER_BUS)

    def test_get_firmware_version(self):
        result = self.serialport.sendGetFirmware()
        print(result)
        self.assertEqual(result[0], 1)
        self.assertEqual(result[1], 0)


if __name__ == '__main__':
    unittest.main()
