import concurrent
import logging

from apscheduler.schedulers.asyncio import AsyncIOScheduler
import ELinkLog

from ElinkScript import ELinkInfo
from eLink.elinkScriptUtils import *

try:
    import asyncio
except ImportError:
    import trollius as asyncio
elinkkvm_toshiba_emmc = "Elink-KVM-44:a3:bd"
elinkkvm_sandisk_emmc = "Elink-KVM-44:a3:b1"  # TODO add sandisk Elinkkvm Id here

testScheduler = AsyncIOScheduler()
testScheduler.start()
count = 0

testlog = logging.getLogger('elinktest')
logger = logging.getLogger('elinkdebug')


def connect_disconnect(dev: ELinkInfo):
    testlog.info("{}".format(connect_disconnect.__name__),
                 extra={'devid': "{}".format(dev.devId),
                        'testid': test_connect_disconnect.__name__,
                        'info': 1}
                 )
    elinkObj = elink.newConnection(dev.ipAddr)
    sleep(2)  # TODO fixed me why need timeout here.
    elinkObj.close()
    return


async def test_connect_disconnect(dev: ELinkInfo):
    global count
    executor = concurrent.futures.ThreadPoolExecutor(max_workers=3)
    loop = asyncio.get_event_loop()
    await loop.run_in_executor(executor, connect_disconnect, dev)
    logger.critical("Connect/Disconnect dev {}  {} {}".format(dev.devId, dev.ipAddr, count))
    count += 1
    if count >= 2000000:
        testScheduler.shutdown()


if __name__ == '__main__':
    print("start scan devices")
    devs_info = elink.scanDevice()
    print("Found {}".format(len(devs_info)))
    if len(devs_info) == 0:
        logger.info("Not found any dev")
        exit(0)
    for each in devs_info:
        dev = ELinkInfo(each)
        logger.info("{}".format(each))
        if dev.devId == elinkkvm_toshiba_emmc:
            logger.info("run test case for {}".format(elinkkvm_toshiba_emmc))
            testScheduler.add_job(test_connect_disconnect, 'interval', seconds=4, id="Emmc Toshiba Test", args=[dev],
                                  max_instances=1)
        else:
            logger.info("run test case for {}".format(elinkkvm_sandisk_emmc))
            testScheduler.add_job(test_connect_disconnect, 'interval', seconds=4, id="Emmc Sandisk Test ",
                                  max_instances=1,
                                  args=[dev])
        # else:
        #     logger.info("not found any devices for the test")
    try:
        loop = asyncio.get_event_loop().run_forever()
    except KeyboardInterrupt:
        # Canceling pending tasks and stopping the loop
        asyncio.gather(*asyncio.Task.all_tasks()).cancel()
        # Stopping the loop
        loop.stop()
        # Received Ctrl+C
        loop.close()
