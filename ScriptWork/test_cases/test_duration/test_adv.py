import sys,os
import ElinkScript
import ELinkLog
from ElinkScript import *
from ElinkScript.Debug import *
from ElinkScript.ElinkSystem import *
from time import sleep
sys.path.append(os.getcwd())
print("cwd: " + os.getcwd())

ElinkSystem.setElink(elink)
vnc = ElinkSystem.connect()
screen = ElinkSystem.getScreen()
print("IP: " + ElinkSystem.getIP())

testlog = logging.getLogger('elinktest')
logger = logging.getLogger('elinkdebug')

def configure_ElinkScript():
    vnc.setVncMode("MODE_VNC_MSC")
    vnc.clrEvent()
    vnc.setUsbMode("USB_MODE_KEY|USB_MODE_VNC_HID",0,["A:\win2012.hdd2"])
    vnc.setKeyMode(1)
    vnc.setMouseMode(2)
    print(" - Viewer configured and Windows 2012 setup image mounted")

def main():
        print("re-enable keyboard")
        vnc.setUsbMode(0,0) #this workaround the bug of KeyOnPower
        time.sleep(2)
        vnc.setUsbMode("USB_MODE_KEY",0) #this workaround the bug of KeyOnPower
        vnc.setKeyMode(1)

        print("\nMonitoring ADV output")
        configure_ElinkScript()

        dev = ElinkSystem.getDeviceInfo(0)
        while true:
            testlog.info("write erase emmc",
                         extra={'devid': "{}".format(dev.devId),
                        'testid': "test_adv",
                        'info': 1}
                 )


if __name__ == "__main__":
        import time
        main()
