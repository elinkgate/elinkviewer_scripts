import concurrent

from ElinkScript import ELinkInfo
from eLink.elinkScriptUtils import *
from PowerMeter.PowerMeter import *
from apscheduler.schedulers.asyncio import AsyncIOScheduler

try:
    import asyncio
except ImportError:
    import trollius as asyncio

default_elinkvkm = "10.42.0.2"
serial_port = "COM3"
action = "None"

testScheduler = AsyncIOScheduler()
powermeter = None

testScheduler.start()
# powermeter.start()
count = 0

testlog = logging.getLogger('elinktest')
logger = logging.getLogger('elinkdebug')


def getPowerMeterChannel():
    powermeter.sendGetDataChannel(InaChannel.PRIMARY_POWER_BUS, action)
    powermeter.sendGetDataChannel(InaChannel.SECONDARY_POWER_BUS, action)


def power_on():
    powermeter.sendSetSwChannel(InaChannel.PRIMARY_POWER_BUS, 0x01)
    powermeter.sendSetSwChannel(InaChannel.SECONDARY_POWER_BUS, 0x01)


def power_off():
    powermeter.sendSetSwChannel(InaChannel.PRIMARY_POWER_BUS, 0x00)
    powermeter.sendSetSwChannel(InaChannel.SECONDARY_POWER_BUS, 0x00)


async def vnc_switch_mode():
    global count
    global new_ramdom
    global action
    power_on()
    powermeter.getPowerInfo(vnc_switch_mode.__name__, Action.POWER_ON, ActionState.ACTION_START)
    WaitForELinkKVMReady(default_elinkvkm)
    elinkObj = elink.newConnection(default_elinkvkm)
    sleep(1)
    powermeter.getPowerInfo(vnc_switch_mode.__name__, Action.FILE_TRANSFER, ActionState.ACTION_START)
    elinkObj.setVncMode(VncMode.VNC_MODE_RGB)
    powermeter.getPowerInfo(vnc_switch_mode.__name__, Action.SWITCH_MODE_VNC, ActionState.ACTION_START,
                            detail="{}".format(VncMode.VNC_MODE_RGB))
    sleep(20)
    elinkObj.close()
    sleep(1)
    powermeter.getPowerInfo(vnc_switch_mode.__name__, Action.SWITCH_MODE_VNC, ActionState.ACTION_FINISH,
                            detail="{}".format(VncMode.VNC_MODE_RGB))
    power_off()
    powermeter.getPowerInfo(vnc_switch_mode.__name__, Action.POWER_OFF, ActionState.ACTION_FINISH)
    count += 1
    # exit


def on_off_vga(dev: ELinkInfo):
    sleep(1)  # TODO fixed me why need timeout here.
    global count
    global new_ramdom
    global action
    power_on()
    WaitForELinkKVMReady(dev.ipAddr)
    powermeter.getPowerInfo(vnc_switch_mode.__name__, Action.POWER_ON, ActionState.ACTION_START)
    elinkObj = elink.newConnection(dev.ipAddr)
    sleep(1)
    powermeter.getPowerInfo(vnc_switch_mode.__name__, Action.FILE_TRANSFER, ActionState.ACTION_START)
    elinkObj.setVncMode(VncMode.VNC_MODE_RGB)
    powermeter.getPowerInfo(vnc_switch_mode.__name__, Action.SWITCH_MODE_VNC, ActionState.ACTION_START,
                            detail="{}".format(VncMode.VNC_MODE_RGB))
    sleep(20)
    elinkObj.close()
    sleep(1)
    testlog.info("test ADV ",
                 extra={'devid': "{}".format(dev.devId),
                        'testid': on_off_vga.__name__,
                        'info': 20}
                 )
    powermeter.getPowerInfo(vnc_switch_mode.__name__, Action.SWITCH_MODE_VNC, ActionState.ACTION_FINISH,
                            detail="{}".format(VncMode.VNC_MODE_RGB))
    power_off()
    powermeter.getPowerInfo(vnc_switch_mode.__name__, Action.POWER_OFF, ActionState.ACTION_FINISH)
    count += 1


async def test_write_erase_emmc(dev: ELinkInfo):
    global count
    testlog.info("write erase emmc",
                 extra={'devid': "{}".format(dev.devId),
                        'testid': test_write_erase_emmc.__name__,
                        'info': 1}
                 )
    executor = concurrent.futures.ThreadPoolExecutor(max_workers=3)
    loop = asyncio.get_event_loop()
    entry = await loop.run_in_executor(executor, on_off_vga, dev)
    sleep(1)
    logger.info("upload new entry {}".format(entry))
    logger.critical("EMMC dev {}  {} {}".format(dev.devId, dev.ipAddr, count))
    count += 1
    if count >= 20000:
        testScheduler.shutdown()


if __name__ == '__main__':
    comports = PowerMeterController.scan_com_port()
    if len(comports) != 0:
        for each in comports:
            powermeter = PowerMeterController(name="test Power", port=each)
    else:
        print("not found any comport ")
        input("Press any key for exit")
        exit(-1)
    logger.info("Start scan device")
    devs_info = elink.scanDevice()
    logger.info("Found {}".format(len(devs_info)))
    if len(devs_info) == 0:
        logger.info("Not found any dev")
        input("Press any key for exit")
        exit(0)
    for each in devs_info:
        dev = ELinkInfo(each)
        logger.info("{}".format(each))
        logger.info("run test case for {}".format(dev.devId))
        testScheduler.add_job(test_write_erase_emmc, 'interval', seconds=10, id="Emmc test {}".format(dev.devId),
                              args=[dev],
                              max_instances=1)
    # testScheduler.add_job(getPowerMeterChannel, 'interval', seconds=0.3, max_instances=3)
    try:
        asyncio.get_event_loop().run_forever()
    except (KeyboardInterrupt, SystemExit):
        pass
