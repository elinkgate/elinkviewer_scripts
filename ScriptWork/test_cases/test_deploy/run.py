from ElinkScript import ELinkInfo
from Utility.ELinkDeploy import ELinkDeploy

if __name__ == '__main__':
    deployer = ELinkDeploy(ELinkInfo(['10.42.0.52', 'Kevin test device']),
                           usrname='admin',
                           password='admin')
    deployer.deploy()
