import logging
import elink

from Utility.ELinkTemp import ELinkTemp, ProbeTemp
from Utility.FileManager import FileManager
from Utility.ELinkUtils import *
from ElinkScript import ELinkInfo
from ElinkScript.TestRunner import *
import ELinkLog

file_size = (1024 * 1024 * 1024 * 1024)  # 1MB
test_file = "test.bin"

testlog = logging.getLogger('elinktest')
logger = logging.getLogger('elinkdebug')


def check_test_file_exist():
    if os.path.isfile(test_file):
        return True
    else:
        create_upload_file()
        if os.path.isfile(test_file):
            return True
        else:
            Exception("not foundction ReformatCode test file")


def create_upload_file():
    f = open(test_file, 'wb')
    f.seek(1073741824 - 1)  # 1073741824 bytes in 1 GiB
    f.write(bytes('abc\0'.encode(encoding='UTF-8')))
    f.close()


def remove_and_upload_file(*args):
    dev: ELinkInfo = args[0]
    elinkObj = elink.newConnection(dev.ipAddr)
    tempmonitor = ELinkTemp(elinkObj)
    tempmonitor.start()
    logger.info("{} typ of elink Obj".format(type(elinkObj)))
    sleep(1)  # TODO fixed me why need timeout here.
    filemnger = FileManager(elinkObj, dev)
    if not check_test_file_exist():
        Exception("Test Error")
    filemnger.upload_file(test_file,
                          "/A:")
    testlog.info("write erase emmc",
                 extra={'devid': "{}".format(dev.devId),
                        'testid': "test_write_erase_emmc",
                        'info': 1,
                        }
                 )
    tempmonitor.join()


if __name__ == '__main__':
    app = TestRunner()
    logger.info("Scanning eLinkKVM device ")
    devs_info = elink.scanDevice()
    logger.info("Found {}".format(len(devs_info)))
    if len(devs_info) == 0:
        logger.info("Not found any dev")
        exit(0)
    count = 0
    for each in devs_info:
        dev = ELinkInfo(each)
        logger.info("{}".format(each))
        logger.info("run test case for {}".format(dev.devId))
        app.add_test(remove_and_upload_file, dev, timeout=5)
    app.run()
