import random
import string
import ELinkLog
from ElinkScript import ELinkInfo
from ElinkScript.TestRunner import *
from Utility.ELinkUsb import *
from Utility.ELinkUtils import *

logger = logging.getLogger('elinkdebug')
testlog = logging.getLogger('elinktest')
ecriticallog = logging.getLogger('ecriticallog')


def test_switch_usb_mode(*args):
    dev: ELinkInfo = args[0]
    elinkObj = elink.newConnection(dev.ipAddr)
    elinkUsb = ELinkUsb(elinkObj)
    for each in UsbId:
        usb_mode = each.value['mode']
        dev_info = elinkUsb.swith_usb_mode(usbmode=usb_mode)
        ret = elinkUsb.wait_for_dev_exist(dev_info)
        if ret < 0:
            exception = "Not found usb (VID/PID}".format(usb_mode.name)
            ecriticallog.critical("Exception => restart dev".format(dev.get_dev_id()), extra={"devid": dev.get_dev_id(),
                                                                                              "testid": "test_on_off_vga_dev",
                                                                                              "ipaddr": dev.get_dev_ip(),
                                                                                              "exception": exception})
            pass
    sleep(1)
    elinkObj.close()
    testlog.info("test switch usb mode",
                 extra={'devid': "{}".format(dev.devId),
                        'testid': test_switch_usb_mode.__name__,
                        'info': 1,
                        }
                 )


if __name__ == '__main__':
    app = TestRunner()
    logger.info("Scanning eLinkKVM device ")
    devs_info = elink.scanDevice()
    logger.info("Found {}".format(len(devs_info)))
    if len(devs_info) == 0:
        logger.info("Not found any dev")
        input("Press any key for exit")
        exit(-1)
    for each in devs_info:
        dev = ELinkInfo(each)
        logger.info("{}".format(each))
        logger.info("run test case for {}".format(dev.devId))
        app.add_test(test_switch_usb_mode, dev, testid=dev.devId)
    app.run()
