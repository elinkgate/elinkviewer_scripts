import sys
import os

sys.path.append(os.getcwd())
from elinkScriptUtils import *

vnc = elink.newConnection("10.42.0.2")


# vnc.ipmiConnect("10.42.0.100", "ADMIN", "ADMIN")


def waitImage3s(vnc, img, score=0.9):
    return waitImage(vnc, img)


def waiting_for_usb_enumeration():
    print(" - Checking for usb enumeration")

    while True:
        e = vnc.getEvent()
        d = e.getData("test")
        dbg("event: " + str(e.getIdCode()))
        dbg("data: " + str(d))
        if e.getIdCode() == 5 and d == 0:
            break


def enter_elinkkvm_usb():
    print(" - Checking for usb enumeration")

    while True:
        e = vnc.getEvent()
        d = e.getData("test")
        dbg("event: " + str(e.getIdCode()))
        dbg("data: " + str(d))
        if e.getIdCode() == 5 and d == 0:
            dbg("sending F12")
            vnc.sendKey("F12")
            break

    print(" -  Check keyboard ready")
    print(" - Select boot usb for Windows setup")
    dbg("Done sending F12 key...")
    sleep(5)
    for i in range(15):
        dbg("send key down")
        sleep(0.2)
        vnc.sendKey("Down")
    for i in range(4):
        dbg("send key up")
        sleep(0.2)
        vnc.sendKey("Up")
    vnc.sendKey("Enter")


def configure_elinkvnewer_setup_redhatserver():
    vnc.setVncMode("MODE_VNC_MSC")
    vnc.clrEvent()
    vnc.setUsbMode("USB_MODE_KEY|USB_MODE_VNC_HID", 0, ["A:\\redhat.hdd2"])
    vnc.setKeyMode(1)
    vnc.setMouseMode(2)
    vnc.setKeyIdle(300)
    print(" - Viewer configured and redhat server setup image mounted")
    return vnc


def linux_setup_set_password():
    waitImage3s(vnc, "13_linux_set_passwd11.png")
    vnc.sendString(" ")
    sleep(1)
    vnc.sendKey("Enter")
    waitImage3s(vnc, "14_linux_set_passwd_cfm.png")
    vnc.sendString(" ")
    vnc.sendKey("Enter")
    waitImage3s(vnc, "15_linux_set_passwd_weak.png")
    vnc.sendKey("Left")
    sleep(1)
    vnc.sendKey("Enter")
    waitImage3s(vnc, "16_linux_set_encrypt_home.png")
    vnc.sendKey("Enter")


def linux_setup_set_clock():
    waitImage3s(vnc, "17_linux_config_clock.png", 0.85)
    vnc.sendKey("Enter")


def linux_setup_disk_partion():
    waitImage3s(vnc, "18_parition_disk1.png")
    waitImage3s(vnc, "18_partition_disk11.png")
    vnc.sendKey("Left")
    vnc.sendKey("Enter")
    force_uefi = 0
    found = waitImagesOr(vnc, ["19_paritition_disk_method.png", "25_partition_disk_force_uefi.png"])
    if found[0] == "19_paritition_disk_method.png":
        vnc.sendKey("Enter")
    elif found[0] == "25_partition_disk_force_uefi.png":
        force_uefi = 1
        vnc.sendKey("Left")
        sleep(1)
        vnc.sendKey("Enter")
        waitImage3s(vnc, "19_paritition_disk_method.png")
        vnc.sendKey("Enter")
    else:
        print(" not found anything")
    waitImage3s(vnc, "20_partition_disk_select_disk.png")
    vnc.sendKey("Enter")

    found = waitImagesOr(vnc, ["21_partition_disk_LVM_existed.png", "21_partition_disk_wr_config.png"])
    sleep(3)
    if found[0] == "21_partition_disk_LVM_existed.png":
        print(" - Found Windows Boot Manager menu")
        print(" - Already existed LVM logical volumes")
        vnc.sendKey("Left")
        vnc.sendKey("Enter")
    elif found[0] == "21_partition_disk_wr_config.png":
        print(" - Found \"Loading Files...\"")
    else:
        print(" not found anything exit")
    waitImage3s(vnc, "21_partition_disk_wr_config.png")
    vnc.sendKey("Left")
    sleep(1)
    vnc.sendKey("Enter")
    waitImage3s(vnc, "22_partition_disk_group_parititon.png")
    vnc.sendKey("Enter")
    if force_uefi == 0:
        print("Not process uefi - force yet")
        waitImage3s(vnc, "25_partition_disk_force_uefi.png")
        vnc.sendKey("Left")
        sleep(1)
        vnc.sendKey("Enter")
    waitImage3s(vnc, "25_partition_disk_write_change.png")
    vnc.sendKey("Left")
    sleep(3)
    vnc.sendKey("Enter")
    waitImage3s(vnc, "26_config_task.png")
    vnc.sendKey("Enter")
    waitImage3s(vnc, "27_software_select.png")
    vnc.sendKey("Enter")
    waitImage3s(vnc, "28_installation_complete.png")
    vnc.sendKey("Enter")


def redhat_server_setup():
    waitImage3s(vnc, "redhat_1_boot_seclect.png")
    vnc.sendKey("Up")
    vnc.sendKey("Enter")
    waitImage3s(vnc, "redhat_2_welcome.png")
    clickbutton(vnc, "redhat_03_click_continuous.png")
    clickbutton(vnc, "redhat_5_click_keyboard.png")
    clickbutton(vnc, "redhat_7_click_done.png")

    print("select software")
    clickbutton(vnc, "redhat_8_click_software.png")
    clickbutton(vnc, "redhat_9_click_mininstall.png")
    clickbutton(vnc, "redhat_10_click_done.png")

    print("click disk partition")
    clickbutton(vnc, "redhat_12_click_disk_format.png")
    clickbutton(vnc, "redhat_12_click_automation_partition.png")
    clickbutton(vnc, "redhat_10_click_done.png")
    clickbutton(vnc, "redhat_18_click_claim_space.png")
    clickbutton(vnc, "redhat_16_click_delete_disk_all.png")
    clickbutton(vnc, "redhat_18_click_claim_space.png")

    print("click time select ")
    clickbutton(vnc, "redhat_19_click_select_time.png")
    clickbutton(vnc, "redhat_20_click_HCMCity.png")
    clickbutton(vnc, "redhat_10_click_done.png")

    clickbutton(vnc, "redhat_22_click_begin_installation.png")

    print("set Root password")
    clickbutton(vnc, "redhat_23_click_set_root_passwd.png")
    vnc.sendKey(" ")
    vnc.sendKey("Tab")
    vnc.sendKey(" ")
    sleep(2)
    clickbutton(vnc, "redhat_10_click_done.png")
    clickbutton(vnc, "redhat_10_click_done.png")

    print("Set User name - password")
    clickbutton(vnc, "redhat_21_click_set_usrname_pass.png")
    vnc.sendString("Kevin Truong")
    vnc.sendKey("Tab")
    vnc.sendString("kevinelg")
    vnc.sendKey("Tab")
    vnc.sendKey("Tab")
    vnc.sendKey("Tab")
    vnc.sendKey(" ")
    vnc.sendKey("Tab")
    vnc.sendKey(" ")
    clickbutton(vnc, "redhat_10_click_done.png")
    clickbutton(vnc, "redhat_10_click_done.png")
    clickbutton(vnc, "redhat_25_click_reboot.png")

    pass


def remount_ubuntu_setup_after_reject():
    waiting_for_usb_enumeration()
    vnc.setUsbMode(0, 0)  # this workaround the bug of KeyOnPower
    sleep(2)
    vnc.setUsbMode("USB_MODE_KEY|USB_MODE_VNC_HID", 0, ["A:\\ubuntu.hddx"])
    vnc.sendKeyEx(["LeftControl", "LeftAlt", "DeleteForward"])
    waiting_for_usb_enumeration()


def server_ubuntu_server_config():
    vnc.setVncMode("MODE_VNC_CAMERA")
    vnc.clrEvent()
    vnc.setUsbMode(0x041, 0, ["A:\\ubuntu.hddx"])
    vnc.setKeyMode(1)
    vnc.setMouseMode(3)
    vnc.setKeyIdle(100)
    print(" - Viewer configured for Windows 2012 setup")


def main():
    print("re-enable keyboard")
    time.sleep(2)
    vnc.setUsbMode("USB_MODE_KEY", 0)  # this workaround the bug of KeyOnPower
    time.sleep(2)
    vnc.setKeyMode(1)
    waitKeyReady(vnc)
    for i in range(1):
        vnc.sendKeyEx(["LeftControl", "LeftAlt", "DeleteForward"])
        configure_elinkvnewer_setup_redhatserver()
        enter_elinkkvm_usb()
        vnc.clrEvent()
        redhat_server_setup()
        vnc.clrEvent()
        sleep(2)
        waiting_for_usb_enumeration()
        vnc.setUsbMode("USB_MODE_KEY", 0)  # this workaround the bug of KeyOnPower
        time.sleep(2)
        vnc.setKeyMode(1)
        vnc.setUsbMode("USB_MODE_KEY|USB_MODE_VNC_HID", 0, ["A:\\redhat.hdd2"])
        vnc.sendKeyEx(["LeftControl", "LeftAlt", "DeleteForward"])


def enter_elinkkvm_usb_server():
    print(" - Waiting for BIOS boot menu")
    loc = [None, None, 0]
    while loc[0] == None:
        loc = vnc.matchScreen("server_bios_menu.png")
        sleep(0.7)
        vnc.sendKey("F11")
    print(" - Found boot menu")

    print(" - Select eLinkKVM USB")
    loc = [None, None, 0]
    while loc[0] == None:
        loc = vnc.matchScreen("server_elinkkvm_selected.png", 0.95)
        if loc[0]:
            sleep(3)
            break
        else:
            vnc.sendKey("Down")
        sleep(0.5)
    print(" - eLinkKVM USB selected")
    #
    vnc.sendKey("Enter")
    vnc.setUsbMode(0x41, 0, ["A:\\ubuntu.hddx"])
    vnc.clrEvent()
    vnc.setKeyMode(1)
    vnc.setMouseMode(3)
    vnc.setKeyIdle(300)


def linux_setup_run():
    redhat_server_setup()
    sleep(2)
    waiting_for_usb_enumeration()
    vnc.setUsbMode("USB_MODE_KEY", 0)  # this workaround the bug of KeyOnPower
    time.sleep(2)
    vnc.setKeyMode(1)
    vnc.setUsbMode("USB_MODE_KEY|USB_MODE_VNC_HID", 0, ["A:\\ubuntu.hddx"])
    vnc.sendKeyEx(["LeftControl", "LeftAlt", "DeleteForward"])


def server_setup():
    print("Reset Server")
    vnc.ipmiReset(0)
    sleep(5)
    print("\nSTAGE 1: Configure the viewer")
    server_ubuntu_server_config()
    print("\nSTAGE 2: Enter boot USB from eLinkKVM")
    enter_elinkkvm_usb_server()
    vnc.setVncMode("MODE_VNC_MSC")


def run_setup_ubuntu_for_server():
    server_setup()
    linux_setup_run()
    pass


if __name__ == "__main__":
    import time

    # run_setup_ubuntu_for_server()
    main()
