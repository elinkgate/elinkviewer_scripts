from ElinkScript.TestRunner import *
import logging
import elink
import ELinkLog

from Utility.ELinkUsb import ELinkUsb
from Utility.ELinkUsbHost import ELinkUsbHostMsc
from Utility.FileManager import FileManager
from Utility.ELinkUtils import *

file_size = (1024 * 1024 * 1024 * 1024)  # 1MB
test_file = "test.bin"

testlog = logging.getLogger('elinktest')
logger = logging.getLogger('elinkdebug')


def check_test_file_exist():
    if os.path.isfile(test_file):
        return True
    else:
        create_upload_file()
        if os.path.isfile(test_file):
            return True
        else:
            Exception("not foundction ReformatCode test file")


def create_upload_file():
    f = open(test_file, 'wb')
    f.seek(1073741824 - 1)  # 1073741824 bytes in 1 GiB
    f.write(bytes('abc\0'.encode(encoding='UTF-8')))
    f.close()


def upload_file_usbhost_msc(*args):
    dev: ELinkInfo = args[0]
    testlog.debug("usb host write file",
                  extra={'devid': "{}".format(dev.devId),
                         'testid': upload_file_usbhost_msc.__name__,
                         'info': 1,
                         }
                  )
    elinkObj = elink.newConnection(dev.ipAddr)
    ELinkUsbHostMsc(elinkobj=elinkObj, devinf=dev).reconfigure_usbhost()

    logger.info("{} typ of elink Obj".format(type(elinkObj)))
    sleep(1)  # TODO fixed me why need timeout here.
    filemnger = FileManager(elinkObj, devinfo=dev)
    if not check_test_file_exist():
        Exception("Test Error")
    filemnger.upload_file(test_file,
                          "/A:")
    elinkObj.close()


if __name__ == '__main__':
    app = TestRunner()
    logger.info("Scanning eLinkKVM device ")
    devs_info = elink.scanDevice()
    logger.info("Found {}".format(len(devs_info)))
    if len(devs_info) == 0:
        logger.info("Not found any dev")
        exit(0)
    count = 0
    for each in devs_info:
        dev = ELinkInfo(each)
        logger.info("{}".format(each))
        logger.info("run test case for {}".format(dev.devId))
        app.add_test(upload_file_usbhost_msc, dev)
    app.run()
